# Swedish debconf translation of cpu.
# Copyright (C) 2012 cpu & nedenstående oversættere.
# This file is distributed under the same license as the cpu package.
# Joe Hansen <joedalton2@yahoo.dk>, 2012
#
msgid ""
msgstr ""
"Project-Id-Version: cpu\n"
"Report-Msgid-Bugs-To: cpu@packages.debian.org\n"
"POT-Creation-Date: 2009-11-04 07:09+0100\n"
"PO-Revision-Date: 2012-08-11 22:39+0100\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <debian-l10n-danish@lists.debian.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../templates:1001
msgid "Do you want to manage cpu's configuration through debconf?"
msgstr "Ønsker du at håndtere cpu's konfiguration igennem debconf?"

#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"Please confirm if you want to allow debconf to manage some parts of your cpu."
"conf. Please note that any further manual changes to cpu.conf will never be "
"overwritten by debconf."
msgstr ""
"Bekræft venligst om du ønsker at tillade, at debconf håndterer dele af din "
"cpu.conf. Bemærk venligst at yderligere manuelle ændringer til cpu.conf aldrig "
"vil blive overskrevet af debconf."

#. Type: string
#. Description
#: ../templates:2001
msgid "LDAP server:"
msgstr "LDAP-server:"

#. Type: string
#. Description
#: ../templates:2001
msgid ""
"Please insert the URI of the LDAP server you plan to use with CPU. Use the "
"standard form of \"ldap[s]://host[:port]\". The default port value is 389. "
"Use ldaps if you intend to use a TLS encrypted connection."
msgstr ""
"Indsæt venligst URI'en for LDAP-serveren, du ønsker at bruge med CPU. Brug "
"standardformatet »ldap[s]://vært[:port]«. Standardporten er 389, brug "
"ldaps hvis du forventer at bruge en TLS-krypteret forbindelse."

#. Type: string
#. Description
#: ../templates:3001
msgid "Base DN of your user subtree:"
msgstr "Basis-DN for dit brugerundertræ:"

#. Type: string
#. Description
#: ../templates:3001
msgid ""
"Please enter the DN of the part of your directory that contains the users "
"you wish to manage with CPU."
msgstr ""
"Indtast venligst DN'en for den del af din mappe som indeholder brugerne, "
"du ønsker at håndtere med CPU."

#. Type: string
#. Description
#: ../templates:4001
msgid "Base DN of your group subtree:"
msgstr "Basis-DN for dit brugerundertræ:"

#. Type: string
#. Description
#: ../templates:4001
msgid ""
"Please enter the DN of the part of your directory that contains the groups "
"you wish to manage with CPU."
msgstr ""
"Indtast venligst DN'en for den del af din mappe som indeholder grupperne, "
"du ønsker at håndtere med CPU."

#. Type: string
#. Description
#: ../templates:5001
msgid "LDAP user DN:"
msgstr "Bruger-DN for LDAP:"

#. Type: string
#. Description
#: ../templates:5001
msgid ""
"Please insert the DN of the user CPU will bind to the LDAP server with. "
"Usually this will be your LDAP admin DN, but can be any other DN, as long as "
"it is configured to have full control over at least the subtree under the "
"base you selected before."
msgstr ""
"Indsæt venligst DN'en for brugeren som CPU vil forbinde til LDAP-serveren med. "
"Det vil normalt være din LDAP admin-DN, men kan være enhver DN, så længe "
"den er konfigureret til at have fuld kontrol over i det mindste undertræet "
"under basisen du valgte før."

#. Type: string
#. Description
#: ../templates:5001
msgid "Example: \"cn=admin,dc=domain,dc=tld\""
msgstr "Eksempel: »cn=admin,dc=domain,dc=tld«"

#. Type: password
#. Description
#: ../templates:6001
msgid "LDAP password:"
msgstr "LDAP-adgangskode:"

#. Type: password
#. Description
#: ../templates:6001
msgid ""
"Please enter the password to use when binding to the LDAP directory. Note "
"that this password will be stored in cleartext in your /etc/cpu/cpu.conf "
"file, so don't let that file became readable to anyone you don't want to "
"give the same power of the user cpu will bind with."
msgstr ""
"Indtast venligst adgangskoden der skal bruges for at binde til LDAP-mappen. "
"Bemærk at denne adgangskode vil blive gemt i klartekst i filen /etc/cpu/cpu.conf, "
"så den fil må ikke kunne læses af folk, du ikke ønsker at give samme "
"rettigheder, som brugeren cpu vil binde sig med."

