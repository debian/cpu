/*
     This file is part of CPU
     (C) 2003 Blake Matheny (and other contributing authors)

     CPU is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 2, or (at your
     option) any later version.

     CPU is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CPU; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * This is the main file for CPU
 * @author Blake Matheny
 * @file cpu.c
 **/
#include <stdlib.h>
#include <string.h>
#include "main/cpu.h"

#ifdef CONFIGFILE
char *conf_file = CONFIGFILE;
#else
char * conf_file = "/usr/local/etc/cpu.conf";
#endif

char * method = NULL;

int
main(int argc, char *argv[])
{
  CPU_Method * gmethod = NULL;
  int argct = 0;
  char *argvt[argc];
  int ret = 0;
  int i = 0;

  argct = argc;

  for ( i = 0; i < argc; i++ )
    {
      argvt[i] = strdup(argv[i]);
    }
  argvt[argct] = NULL;

  if ( argc > 1 )
    {
      ret = parseCommand(argc, argv);
      if ( ret < 0 )
	{
#ifdef DEBUG
	  fprintf(stderr, "DEBUG: Error parsing command line. Exiting.\n");
#endif
	  exit(EXIT_FAILURE);
	}
      else if ( ret == 1 )
	exit(EXIT_FAILURE);
    }

  if ( conf_file == NULL )
    {
      fprintf(stderr, "No configuration file could be found. Exiting.\n");
      exit(EXIT_FAILURE);
    }
  if ( cfg_parse_file(conf_file) != 0 )
    {
      fprintf(stderr, "There was an error parsing the configuration file. Exiting.\n");
      exit(EXIT_FAILURE);
    }
  optind = 0;
  if ( method == NULL )
    {
      method = cfg_get_str("GLOBAL", "DEFAULT_METHOD");
      if ( method == NULL )
	{
	  fprintf(stderr, "No method specified. Exiting.\n");
	  exit(EXIT_FAILURE);
	}
    }
  if ( (gmethod = CPU_loadLibrary(method)) == NULL)
    {
      fprintf(stderr, "There was an error loading the %s library. Exiting.\n",
		      method);
      exit(EXIT_FAILURE);
    }
  if ( gmethod != NULL )
    {
      if ( gmethod->method.initialize(argct, argvt) != 0 ) {
	exit(EXIT_FAILURE);
      }
    }
  CPU_unloadLibrary(gmethod);
  exit(EXIT_SUCCESS);
}

int
parseCommand(int argc, char *argv[])
{
  int c = 0;
  int options_index = 0;
  int help_flag = 0;
  int method_flag = 0;
  int version_flag = 0;
  extern int opterr;
  struct option long_options[] = {
    DEFAULT_OPTIONS,
    CPU_OPTS,
    { 0,0,0,0 }
  };

  opterr = 1;
  while (
	(c = cgetopt_long(argc, argv,
"0::1::2::3::4::5::6::7::8::9::A::B::C:D::E::F::G::H::I::J::K::L::M:N::O::P::Q::R::S::T::U::VW::X::Y::Z::a::b::c::d::e::f::g::hi::j::k::l::m::n::o::p::q::r::s::t::u::v::w::x::y::z::",
	long_options, &options_index))
	!= -1)
    {
      switch (c)
	{
	  case 'C':
	    conf_file = NULL;
	    conf_file = strdup(optarg);
	    if ( conf_file == NULL )
	      {
		perror("strdup");
		return -1;
	      }
	    break;
	  case 'h':
	    help_flag = 1;
	    break;
	  case 'M':
	    method_flag = 1;
	    method = strdup(optarg);
	    if ( method == NULL )
	      {
		perror("strdup");
		return -1;
	      }
	    break;
	  case 'V':
	    version_flag = 1;
	    break;
	  default:
	    break;
	}
    }
  if ( help_flag && !method_flag )
    {
      CTEST();
      printDefaultHelp();
      return 1;
    }
  if ( version_flag && !method_flag )
    {
      printDefaultVersion();
      return 1;
    }
  return 0;
}
