/*
     This file is part of CPU
     (C) 2003 Blake Matheny (and other contributing authors)

     CPU is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 2, or (at your
     option) any later version.

     CPU is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CPU; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * this is a 'portable' fgetpwent implementation
 * @author Blake Matheny
 * @file cgetpwent.c
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "conf.h"

#define MAX_LINE 512

/* cgetpwent finds a user in a file and returns their data
 * @filename the file to open
 * @name the username to find
 * @ftype the type of file (shadow or password)
 * @return a structure containing the information or NULL
 */
struct cpass *
cgetpwent(char * filename, char * name, pfile_t ftype)
{
  struct cpass * result;
  FILE * passfile = NULL;
  char * pass_char;

  if ( filename == NULL )
    return NULL;

  if ( name == NULL )
    return NULL;

  if ( ftype != PASSWORD && ftype != SHADOW )
    return NULL;

  result = (struct cpass*)malloc(sizeof(struct cpass));
  if ( result == NULL )
    return NULL;
  bzero(result, sizeof(struct cpass));

  if ( (passfile = fopen (filename, "r")) == NULL)
    return NULL;

  pass_char = (char*)malloc(sizeof(char)*MAX_LINE);
  if ( pass_char == NULL )
    return NULL;
  bzero(pass_char, sizeof(char));

  while (fgets (pass_char, MAX_LINE-1, passfile) != NULL)
    {
      char *m = pass_char;
      int num_tok = 0;
      char *toks;
      char * temp;
      while (m != NULL && *m != 0)
	{
	  toks = getToken (&m, ":");
	  if ( ftype == PASSWORD )
	    {
	      if (num_tok == 0)
		result->pw_name = toks;
	      else if (num_tok == 1)
		result->pw_passwd = toks;
	      else if (num_tok == 2)
		result->pw_uid = atoi(toks);
	      else if (num_tok == 3)
		result->pw_gid = atoi(toks);
	      else if (num_tok == 4)
		result->pw_gecos = (toks == NULL || toks[0] == 0) ? 
		    result->pw_name:toks;
	      else if (num_tok == 5)
		result->pw_dir = (toks == NULL || toks[0] == 0) ?
		    strdup("/"):toks;
	      else if (num_tok == 6)
		{
		  temp = strdup(toks);
		  if ( temp[strlen(temp)-1] == '\n' )
		    temp[strlen(temp)-1] = '\0';
		  result->pw_shell = temp;
		}
	      else
		break;
	    }
	  else if ( ftype == SHADOW )
	    {
	      if (num_tok == 0)
		result->pw_name = toks;
	      else if (num_tok == 1)
		result->pw_passwd = toks;
	      else if (num_tok == 2)
		result->sp_lstchg = atol(toks);
	      else if (num_tok == 3)
		result->sp_min = atoi(toks);
	      else if (num_tok == 4)
		result->sp_max = atoi(toks);
	      else if (num_tok == 5)
		result->sp_warn = atoi(toks);
	      else if (num_tok == 6)
		result->sp_inact = atoi(toks);
	      else if (num_tok == 7)
		result->sp_expire = atoi(toks);
	      else if (num_tok == 8)
		{
		  temp = strdup(toks);
		  if ( temp[strlen(temp)-1] == '\n' )
		    temp[strlen(temp)-1] = '\0';
		  result->sp_flag = atoi(temp);
		}
	      else
		break;
	    }
	  num_tok++;
	}
      if (strcmp (result->pw_name, name) == 0) {
	  break;
      }
      bzero(pass_char, sizeof(char)*MAX_LINE);
    }
  if ( strcmp (result->pw_name, name) != 0 ) {
    result = NULL;
  }
  return result;
}
