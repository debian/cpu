/*
     This file is part of CPU
     (C) 2003 Blake Matheny (and other contributing authors)

     CPU is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 2, or (at your
     option) any later version.

     CPU is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CPU; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * Some memory wrapper routines
 * @author Blake Matheny
 * @file xmem.c
 **/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include "util/xmem.h"

void
Free(void * ptr)
{
  if ( ptr != NULL )
    free(ptr);
  return;
}

char *
Strdup(const char * s, const char * msg, ... )
{
  char * temp;
#ifdef MEM_DEBUG
  va_list ap;
#endif

  if ( s == NULL )
    return NULL;

  temp = strdup(s);

#ifdef MEM_DEBUG
  va_start(ap, msg);
#endif

  if ( temp == NULL )
    {
#ifdef MEM_DEBUG
      vfprintf(stderr, msg, ap);
#endif
      perror("strdup");
    }
  else
    {
#ifdef MEM_DEBUG
      vfprintf(stderr, msg, ap);
#endif
    }

#ifdef MEM_DEBUG
  va_end(ap);
#endif

  return temp;
}
