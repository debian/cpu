/*
     This file is part of CPU
     (C) 2003 Blake Matheny (and other contributing authors)

     CPU is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 2, or (at your
     option) any later version.

     CPU is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CPU; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * helpful routines
 * @author Blake Matheny
 * @file helper.c
 **/
#include "conf.h"
#include <string.h>
#include <ctype.h>
#include <stdio.h>

char *
getToken (char ** str, const char * delims)
{
  char *token;

  if (*str == NULL)
    {
      /* No more tokens */
      return NULL;
    }

  token = *str;
  while (**str != '\0')
    {
      if (strchr (delims, **str) != NULL)
	{
	  **str = '\0';
	  (*str)++;
	  return token;
	}
      (*str)++;
    }
  /* There is no other token */
  *str = NULL;
  return token;
}

/* this doesn't work with 'optional' args for some reason */
char *
ctolower (char * str)
{
  int length = 0;
  int i = 0;
  char *temp = NULL;
  char *temp2 = NULL;

  if ( str == NULL )
    return NULL;

  temp2 = strdup(str);
  if ( temp2 == NULL )
    return NULL;

  length = (strlen(temp2)+1);
  temp = (char *)malloc(sizeof(char)*length);
  if ( temp == NULL )
    return NULL;
  bzero(temp, sizeof(char)*length);

  for (i = 0; i < length; i++)
    temp[i] = tolower(temp2[i]);

  free(temp2);
  return temp;
}

void
printDefaultHelp(void)
{
  fprintf(stderr, "USAGE: cpu [options]\n"\
		  "\t-C CONFIG, --config=CONFIG       : configfile\n"\
		  "\t-h, --help                       : this help\n"\
		  "\t-M METHOD, --method=METHOD       : method\n"\
		  "\t-V, --version                    : cpu version\n");
  return;
}

void
printDefaultVersion(void)
{
  fprintf(stderr, "CPU %s\n"\
		  "Written by Blake Matheny\n"\
		  "Copyright 2001, 2002, 2003\n\n"\
		  "This is free software; see the source for copying "\
		  "conditions. There is NO\nwarranty; not even for "\
		  "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n",
		  VERSION);
  return;
}
