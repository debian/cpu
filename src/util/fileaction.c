/* CPU Change Password Utility
   Copyright (C) 2003 Blake Matheny

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/param.h>		/* definition for MAXPATHLEN */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>

#include "conf.h"
#include "util/fileaction.h"

/* WRITEME: please */
char *
getHomeDir(char * prefix, char * suffix)
{
  return NULL;
}

/* Remove a directory and all contents
 * @param directory the directory to remove
 * @return non-zero indicates failure
 */
int
remdir (char * directory)
{
  DIR *dirp;
  struct dirent *dp;
  struct stat st;
  char *filename = NULL;
  size_t strsize = 0;

  if ((dirp = opendir (directory)) == NULL)
    {
      perror ("opendir");
      return -1;
    }

  while ((dp = readdir (dirp)) != NULL)
    {
      if ((strcmp (dp->d_name, ".") != 0) && (strcmp (dp->d_name, "..") != 0))
	{
	  strsize = strlen(dp->d_name) + strlen(directory) + 2;
	  filename = (char *) malloc (sizeof (char *) * strsize);
	  if ( filename == NULL )
	    return -1;
	  bzero(filename, strsize);
	  snprintf (filename, strsize, "%s/%s", directory, dp->d_name);

	  if (lstat (filename, &st) == -1)
	    {
	      perror ("lstat");
	    }

	  if (S_ISDIR (st.st_mode))
	    {
	      if (rmdir (filename) == -1)
		remdir (filename);
	    }
	  else
	    {
	      if (unlink (filename) == -1)
		{
		  perror ("unlink");
		}
	    }
	  free (filename);
	}
    }
  if (rmdir (directory) == -1)
    {
      perror ("rmdir");
    }

  if (closedir (dirp) == -1)
    {
      fprintf (stderr,
		"Error doing closedir(), probably nothing to worry about\n");
    }

  return 0;
}

/* copy the contents of one directory to another
 * @param directory the directory to copy content from
 * @param newdirectory the directory to copy content to
 * @param pw_uid the uid to use with chown
 * @param pw_gid the gid to use with chown
 * @return non-negative integer indicates failure
 */
int
copy (char * directory, char * newdirectory, uid_t pw_uid, gid_t pw_gid)
{
  DIR *dirp;
  struct dirent *dp;
  struct stat st;
  char *skelfile = NULL;
  char *homefile = NULL;
  int fd1, fd2;
  void *data;
  size_t skelsize = 0;
  size_t homesize = 0;

  if ((dirp = opendir (directory)) == NULL)
    {
      perror ("opendir");
      return -1;
    }

  if ( stat(newdirectory, &st) < 0 )
    {
      if ( errno == ENOENT )
	{
	  /* FIXME: default permissions */
	  if ( mkdir(newdirectory, atoo("0755")) < 0 )
	    {
	      perror("mkdir");
	      return -1;
	    }
	  if ( chown(newdirectory, pw_uid, pw_gid) < 0 )
	    {
	      perror("chown");
	      return -1;
	    }
	}
      else
	{
	  perror("stat");
	  return -1;
	}
    }

  while ((dp = readdir (dirp)) != NULL)
    {
      if ((strcmp (dp->d_name, ".") != 0) && (strcmp (dp->d_name, "..") != 0))
	{
	  skelsize = strlen(dp->d_name)+strlen(directory)+2;
	  homesize = strlen(dp->d_name)+strlen(newdirectory)+2;

	  skelfile = (char *) malloc (sizeof (char *) * skelsize);
	  if ( skelfile == NULL )
	    return -1;
	  bzero(skelfile, skelsize);
	  snprintf (skelfile, skelsize, "%s/%s", directory, dp->d_name);

	  homefile = (char *) malloc (sizeof (char *) * homesize);
	  snprintf (homefile, homesize, "%s/%s", newdirectory, dp->d_name);

	  if (lstat (skelfile, &st) == -1)
	    {
	      perror ("lstat");
	    }

	  if (S_ISDIR (st.st_mode))
	    {
	      /* FIXME: need to get default permissions, not just 744 */
	      mkdir (homefile, atoo ("744"));
	      if (chown (homefile, pw_uid, pw_gid) < 0)
		{
		  perror ("chown");
		}
	      copy (skelfile, homefile, pw_uid, pw_gid);
	    }
	  else
	    {
	      if ((fd1 = open (skelfile, O_RDONLY)) == -1)
		{
		  perror ("open");
		  continue;
		}
	      data = malloc (st.st_size);
	      if (read (fd1, data, st.st_size) == -1)
		{
		  perror ("read");
		  free (data);
		  close (fd1);
		  continue;
		}
	      close (fd1);

	      if ((fd2 = open (homefile, O_CREAT | O_EXCL | O_WRONLY)) == -1)
		{
		  perror ("open");
		  free (data);
		  continue;
		}

	      if (write (fd2, data, st.st_size) == -1)
		perror ("write");
	      close (fd2);
	      free (data);

	      if (chown(homefile, pw_uid, pw_gid) < 0 )
		{
		  perror ("chown");
		}

	      if (chmod (homefile, st.st_mode) == -1)
		{
		  perror ("chmod");
		}

	    }
	  free (skelfile);
	  free (homefile);
	  skelsize = homesize = 0;
	}
    }
  closedir (dirp);

  return 0;
}

int
atoo (char *s)
{
  int n = 0;

  while ('0' <= *s && *s < '8')
    {
      n <<= 3;
      n += *s++ - '0';
    }
  return n;
}
