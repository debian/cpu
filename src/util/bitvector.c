#include "util/bitvector.h"

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// internal utility functions
static int bitvector_firstset_find(bitvector *b, int startat);
static int bitvector_firstunset_find(bitvector *b, int startat);

bitvector *bitvector_create (int size)
{
	bitvector *b;
	int chunks, bytes;

	if (size < 1) size = 1;

	b = (bitvector *) (malloc (sizeof (bitvector)));

	if (b == NULL)
		return NULL;

	/* Calculate number of int-sized chunks to allocate */
	chunks = (size / BV_CHUNKSIZE) + 1;
	bytes = chunks * BV_CHUNKSIZE_BYTES;
	BV_BITS(b) = (unsigned int *)calloc(chunks, BV_CHUNKSIZE_BYTES);
	if (BV_BITS(b) == NULL)
	{
		bitvector_free (b);
		return NULL;
	}

	b->size = bytes << 3;
	b->arraylen = chunks;

	b->firstset = -1;
	b->firstunset = 0;

	return b;
}

bitvector *bitvector_create_dirty (int size)
{
	bitvector *b;
	int chunks, bytes;

	assert (size >= 0);

	b = (bitvector *) (malloc (sizeof (bitvector)));

	if (b == NULL)
		return NULL;

	/* Calculate number of int-sized chunks to allocate */
	chunks = (size / (BV_CHUNKSIZE+1)) + 1;
	bytes = chunks * BV_CHUNKSIZE_BYTES;
	BV_BITS(b) = (unsigned int *)malloc(bytes);
	if (BV_BITS(b) == NULL)
	{
		bitvector_free (b);
		return NULL;
	}
	/* memset (BV_BITS(b), 0, bytes); do not initialize the data */

	b->size = bytes << 3;
	b->arraylen = b->size / BV_CHUNKSIZE;

	b->dirty = true;

	return b;
}

void bitvector_free (bitvector *b)
{
	assert (b != NULL);
	
	if (b->bits)
		free (b->bits);
	free (b);
}

static int bitvector_firstset_find(bitvector *b, int startat)
{
	uint8_t *bits = (uint8_t *)BV_BITS(b);

	int chunk = startat / 8;
	int numchunks = BV_BYTESIZE(b);

	uint8_t empty = 0;

	int i;

	// Outer loop: find the first block that's not fully unset
	for (i=chunk; i < numchunks; i++)
	{
		uint8_t chunk = bits[i];
		if (chunk == empty)
			continue;
		else
		{
			int j;
			// Inner loop: find the first bit inside that block that's set
			for (j=0; j<8; j++)
			{
				// if the j-th bit is not set, return it
				if (chunk & (1<<j))
					return j + (i*8);
			}
		}
	}

	return -1;
}

static int bitvector_firstunset_find(bitvector *b, int startat)
{
	uint8_t *bits = (uint8_t *)BV_BITS(b);

	int chunk = startat / 8;
	int numchunks = BV_BYTESIZE(b);

	uint8_t full = ~0;

	int i;

	// Outer loop: find the first block that's not fully set
	for (i=chunk; i < numchunks; i++)
	{
		uint8_t chunk = bits[i];
		if (chunk == full)
			continue;
		else
		{
			int j;
			// Inner loop: find the first bit inside that block that's unset
			for (j=0; j<8; j++)
			{
				// if the j-th bit is not set, return it
				if (!(chunk & (1<<j)))
					return j + (i*8);
			}
		}
	}

	return -1;
}

void bitvector_set (bitvector *b, unsigned int n)
{
	int chunk, offset;

	assert (b != NULL);
	assert (n >= 0);
	assert (n < BV_BITSIZE(b));

	chunk = n / BV_CHUNKSIZE;
	offset = n - chunk*BV_CHUNKSIZE;

	BV_BITS(b)[chunk] |= 1<<offset;

	if (b->firstset > n || b->firstset == -1)
		b->firstset = n;

	if (b->firstunset == n)
		b->dirty = true;
}

void bitvector_unset (bitvector *b, unsigned int n)
{
	int chunk, offset;

	assert (b != NULL);
	assert (n >= 0);
	assert (n < BV_BITSIZE(b));

	chunk = n / BV_CHUNKSIZE;
	offset = n - chunk*BV_CHUNKSIZE;

	BV_BITS(b)[chunk] &= ~(1<<offset);

	if (b->firstunset > n || b->firstunset == -1)
		b->firstunset = n;

	if (b->firstset == n)
		b->dirty = true;
}

int bitvector_get (bitvector *b, unsigned int n)
{
	int chunk, offset;

	assert (b != NULL);
	assert (n >= 0);
	assert (n < BV_BITSIZE(b));

	chunk = n / BV_CHUNKSIZE;
	offset = n - chunk*BV_CHUNKSIZE;

	return (BV_BITS(b)[chunk] & (1<<offset));
}

void bitvector_tostring (bitvector *b, char *buffer)
{
	int i;

	assert (b != NULL);
	assert (buffer != NULL);

	for (i=0; i<(int)BV_BITSIZE(b); i++)
		buffer[i] = bitvector_get(b,i) ? '1' : '0';
	buffer[i] = '\0';
}

bitvector *bitvector_fromstring (char *s)
{
	int len;
	int i;
	bitvector *b;
	
	assert (s != NULL);
	
	len = strlen (s);
	b = bitvector_create (len);

	for (i=0; i<len; i++)
		if (s[i] == '1')
			bitvector_set (b, i);

	return b;
}

bitvector *bitvector_fromint (int n)
{
	bitvector *b = bitvector_create (0);

	*(BV_BITS(b)) = n;

	b->dirty = true;

	return b;
}

int bitvector_copy (bitvector *src, bitvector *dst)
{
	if (BV_BITSIZE(src) > BV_BITSIZE(dst))
	{
		if (bitvector_resize_ns (dst, BV_BITSIZE(src)))
			return -1;
	}

	dst->dirty = true;

	memcpy (BV_BITS(dst), BV_BITS(src), src->arraylen*BV_CHUNKSIZE_BYTES);
	return 0;
}

bitvector *bitvector_duplicate (bitvector *src)
{
	bitvector *dst = bitvector_create_dirty (BV_BITSIZE(src));
	if (!dst) return NULL;
	memcpy (BV_BITS(dst), BV_BITS(src), src->arraylen*BV_CHUNKSIZE_BYTES);
	dst->dirty = true;
	return dst;
}

int bitvector_resize (bitvector *b, unsigned int n)
{
	int chunks, bytes;
	unsigned int *oldbits;

	assert (b != NULL);
	assert (b->bits != NULL);
	assert (n >= 0);

	chunks = (n / (BV_CHUNKSIZE+1)) + 1;
	bytes = chunks * BV_CHUNKSIZE_BYTES;
	oldbits = BV_BITS(b);
	BV_BITS(b) = (unsigned int *) realloc (oldbits, bytes);
	if (!BV_BITS(b))
	{
		BV_BITS(b) = oldbits;
		return -1;
	}
	b->size = bytes << 3;
	b->arraylen = b->size / BV_CHUNKSIZE;

	if (b->firstset > BV_BITSIZE(b))
		b->firstset = -1;
	if (b->firstunset > BV_BITSIZE(b))
		b->firstunset = -1;

	return 0;
}

int bitvector_resize_ns (bitvector *b, unsigned int n)
{
	int chunks;

	assert (b != NULL);
	assert (b->bits != NULL);
	assert (n >= 0);

	chunks = (n / (BV_CHUNKSIZE+1)) + 1;

	if (BV_BITS(b))
		free (BV_BITS(b));
	BV_BITS(b) = (unsigned int *) (calloc (chunks, BV_CHUNKSIZE_BYTES));

	if (!BV_BITS(b))
	{
		/* Ok, it failed, fine: get us SOMETHING there so the proggy won't
		 * crash. This one isn't checked, because it is assumed to be less than
		 * or equal to the original size of the bitvector, and ergo, cannot
		 * fail. In theory :) */
		(void) bitvector_resize_ns (b, 1);
		return -1;
	}
	
	b->size = (chunks*BV_CHUNKSIZE_BYTES) << 3;
	b->arraylen = b->size / BV_CHUNKSIZE;

	b->dirty = true;

	return 0;
}

int bitvector_or (bitvector *dest, bitvector *lhs, bitvector *rhs)
{
	int i;
	unsigned int *to, *from;
	bitvector *shorter, *longer;

	assert (dest != NULL);
	assert (dest->bits != NULL);
	assert (lhs != NULL);
	assert (lhs->bits != NULL);
	assert (rhs != NULL);
	assert (rhs->bits != NULL);

	if (BV_BITSIZE(lhs) > BV_BITSIZE(rhs))
	{
		longer = lhs;
		shorter = rhs;
	}
	else
	{
		longer = rhs;
		shorter = lhs;
	}

	if (bitvector_copy (longer, dest))
		return -1;
	
	to = BV_BITS(dest);
	from = BV_BITS(shorter);
	for (i=0; i<(int)(shorter->arraylen); i++)
	{
		*to |= *from;
		*to++; *from++;
	}

	dest->dirty = true;

	return 0;
}

int bitvector_oreq (bitvector *lhs, bitvector *rhs)
{
	int i, len;
	unsigned int *to, *from;

	assert (lhs != NULL);
	assert (lhs->bits != NULL);
	assert (rhs != NULL);
	assert (rhs->bits != NULL);

	if (BV_BITSIZE(lhs) < BV_BITSIZE(rhs))
		if (bitvector_resize (lhs, BV_BITSIZE(rhs)))
			return -1;
	
	len = min(lhs->arraylen, rhs->arraylen);

	to = BV_BITS(lhs);
	from = BV_BITS(rhs);
	for (i=0; i<len; i++)
	{
		*to |= *from;
		*to++; *from++;
	}

	lhs->dirty = true;

	return 0;
}

int bitvector_xor (bitvector *dest, bitvector *lhs, bitvector *rhs)
{
	int i;
	unsigned int *to, *from;
	bitvector *shorter, *longer;

	assert (dest != NULL);
	assert (dest->bits != NULL);
	assert (lhs != NULL);
	assert (lhs->bits != NULL);
	assert (rhs != NULL);
	assert (rhs->bits != NULL);

	if (BV_BITSIZE(lhs) > BV_BITSIZE(rhs))
	{
		longer = lhs;
		shorter = rhs;
	}
	else
	{
		longer = rhs;
		shorter = lhs;
	}

	if (bitvector_copy (longer, dest))
		return -1;
	
	to = BV_BITS(dest);
	from = BV_BITS(shorter);
	for (i=0; i<(int)(shorter->arraylen); i++)
	{
		*to ^= *from;
		*to++; *from++;
	}

	dest->dirty = true;

	return 0;
}

int bitvector_xoreq (bitvector *lhs, bitvector *rhs)
{
	int i, len;
	unsigned int *to, *from;

	assert (lhs != NULL);
	assert (lhs->bits != NULL);
	assert (rhs != NULL);
	assert (rhs->bits != NULL);

	if (BV_BITSIZE(lhs) < BV_BITSIZE(rhs))
		if (bitvector_resize (lhs, BV_BITSIZE(rhs)))
			return -1;
	
	len = min(lhs->arraylen, rhs->arraylen);

	to = BV_BITS(lhs);
	from = BV_BITS(rhs);
	for (i=0; i<len; i++)
	{
		*to ^= *from;
		*to++; *from++;
	}

	lhs->dirty = true;

	return 0;
}

/* Computes dest = lhs & rhs. Note that this function only actually compares
 * and stores the length of the shortest operand. This is because since it is
 * an AND operation, we'd never actually need to compute the results of the
 * rest. */
int bitvector_and (bitvector *dest, bitvector *lhs, bitvector *rhs)
{
	int size, i;
	unsigned int *to, *from1, *from2;

	assert (dest != NULL);
	assert (dest->bits != NULL);
	assert (lhs != NULL);
	assert (lhs->bits != NULL);
	assert (rhs != NULL);
	assert (rhs->bits != NULL);

	size = min(BV_BITSIZE(lhs), BV_BITSIZE(rhs));
	if ((int)BV_BITSIZE(dest) < size)
		if (bitvector_resize_ns (dest, size))
			return -1;

	to = BV_BITS(dest);
	from1 = BV_BITS(lhs);
	from2 = BV_BITS(rhs);

	for (i=0; i<(int)(dest->arraylen); i++)
	{
		*to = *from1 & *from2;
		to++;
		from1++;
		from2++;
	}

	dest->dirty = true;

	return 0;
}

void bitvector_andeq (bitvector *lhs, bitvector *rhs)
{
	int i, len;
	unsigned int *to, *from;

	assert (lhs != NULL);
	assert (lhs->bits != NULL);
	assert (rhs != NULL);
	assert (rhs->bits != NULL);

	to = BV_BITS(lhs);
	from = BV_BITS(rhs);

	len = min (lhs->arraylen, rhs->arraylen);
	for (i=0; i<len; i++)
	{
		*to &= *from;
		to++;
		from++;
	}
	if (i < (int)(lhs->arraylen))
		memset (to, 0, (lhs->arraylen-i)*BV_CHUNKSIZE_BYTES);

	lhs->dirty = true;
}

int bitvector_not (bitvector *nb, bitvector *b)
{
	int i;
	unsigned int *to, *from;

	assert (nb != NULL);
	assert (nb->bits != NULL);
	assert (b != NULL);
	assert (b->bits != NULL);

	if (BV_BITSIZE(nb) < BV_BITSIZE(b))
		if (bitvector_resize_ns (nb, BV_BITSIZE(b)))
			return -1;

	to = BV_BITS(nb);
	from = BV_BITS(b);

	for (i=0; i<(int)(b->arraylen); i++)
	{
		*to = ~(*from);
		to++;
		from++;
	}

	nb->dirty = b->dirty;
	nb->firstset = b->firstunset;
	nb->firstunset = b->firstset;

	return 0;
}

void bitvector_invert (bitvector *b)
{
	int i;
	unsigned int *bits;

	assert (b != NULL);
	assert (b->bits != NULL);

	bits = BV_BITS(b);

	for (i=0; i<(int)(b->arraylen); i++)
	{
		*bits = ~(*bits);
		bits++;
	}

	{
		int tmp;
		tmp = b->firstset;
		b->firstset = b->firstunset;
		b->firstunset = tmp;
	}
}

int bitvector_isempty (bitvector *b)
{
	unsigned int *bits;
	int i;

	assert (b != NULL);
	assert (BV_BITS(b) != NULL);

	bits = BV_BITS(b);

	for (i=0; i<(int)(b->arraylen); i++)
	{
		if (*bits)
			return 0;
		bits++;
	}
	
	return 1;
}

int bitvector_isfull (bitvector *b)
{
	const int full = ~0;
	unsigned int *bits;
	int i;

	assert (b != NULL);
	assert (BV_BITS(b) != NULL);

	bits = BV_BITS(b);
	for (i=0; i<(int)(b->arraylen); i++)
	{
		if ((int)(*bits) != full)
			return 0;
		bits++;
	}

	return 1;
}

/* The extra complexity we see here is because we allow you to compare to
 * bitvectors of differing lengths, and have them be equal. That is, if the
 * longer one is merely trailing zeroes, it is sensible that they are equal.
 * That's why we have the second for loop, ensuring that the padding is merely
 * zeroes. */
int bitvector_isequal (bitvector *a, bitvector *b)
{
	unsigned int *shorter;
	unsigned int *longer;
	int i, slen, llen;
	
	assert (a != NULL);
	assert (BV_BITS(a) != NULL);
	assert (b != NULL);
	assert (BV_BITS(b) != NULL);

	if (a->arraylen > b->arraylen)
	{
		shorter = BV_BITS(b);
		longer = BV_BITS(a);
		slen = b->arraylen;
		llen = a->arraylen;
	} else {
		shorter = BV_BITS(a);
		longer = BV_BITS(b);
		slen = a->arraylen;
		llen = b->arraylen;
	}

	for (i=0; i<slen; i++)
	{
		if (*shorter != *longer)
			return 0;
		shorter++;
		longer++;
	}

	for (; i<llen; i++)
	{
		if (*longer)
			return 0;
		longer++;
	}
	return 1;
}

void bitvector_leftshift (bitvector *b, int n)
{
	unsigned int *bits;
	int i;
	unsigned int overflowmask;
	unsigned int newoverflow, oldoverflow;
	int shift;

	if (n > (int)BV_CHUNKSIZE)
	{
		int sh1, sh2;

		sh1 = n/2;
		sh2 = n - sh1;

		bitvector_leftshift (b, sh1);
		bitvector_leftshift (b, sh2);
		return;
	}

	assert (n >= 0);
	assert (b != NULL);
	assert (BV_BITS(b) != NULL);

	overflowmask = 0;
	for (i=1; i<=n; i++)
		overflowmask |= (1<<(BV_CHUNKSIZE-i));

	oldoverflow = 0;
	shift = BV_CHUNKSIZE-n;

	bits = BV_BITS(b);
	for (i=0; i<(int)(b->arraylen); i++)
	{
		newoverflow = (*bits & overflowmask) >> shift;
		*bits = (*bits << n) | oldoverflow;
		oldoverflow = newoverflow;

		bits++;
	}
	if (oldoverflow)
	{
		bitvector_resize (b, BV_BITSIZE(b)+n);
		bits = BV_BITS(b) + (b->arraylen-1);
		*bits = oldoverflow;
	}

	b->dirty = true;
}

void bitvector_rightshift (bitvector *b, int n)
{
	unsigned int *bits;
	int i;
	unsigned int overflowmask;
	unsigned int newoverflow, oldoverflow;
	int shift;

	if (n > (int)BV_CHUNKSIZE)
	{
		int sh1, sh2;

		sh1 = n/2;
		sh2 = n - sh1;

		bitvector_rightshift (b, sh1);
		bitvector_rightshift (b, sh2);
		return;
	}

	assert (n >= 0);
	assert (b != NULL);
	assert (BV_BITS(b) != NULL);

	overflowmask = 0;
	for (i=0; i<n; i++)
		overflowmask |= 1<<i;

	oldoverflow = 0;
	shift = BV_CHUNKSIZE-n;

	bits = BV_BITS(b) + (b->arraylen-1);
	for (i=0; i<(int)(b->arraylen); i++)
	{
		newoverflow = (*bits & overflowmask) << shift;
		*bits >>= n;
		*bits |= oldoverflow;
		oldoverflow = newoverflow;

		bits--;
	}

	b->dirty = true;
}

void bitvector_clear (bitvector *b)
{
	assert (b!=NULL);
	assert (BV_BITS(b)!=NULL);

	memset (BV_BITS(b), 0, BV_BYTESIZE(b));

	b->firstset = -1;
	b->firstunset = 0;
	b->dirty = 0;
}

void bitvector_print (bitvector *b)
{
	char *s = malloc (sizeof(char)*BV_BITSIZE(b)+1);
	bitvector_tostring (b, s);
	printf (s);
	free (s);
}

/*
   These encoding/decoding functions were written for the SQLite database by D.
   Richard Hipp. They were shamelessly stolen and put here by me to do the job.
   These shouldn't be of concern to anyone: they are used internally only.
   */

/*
** Encode a binary buffer "in" of size n bytes so that it contains
** no instances of characters '\'' or '\000'.  The output is 
** null-terminated and can be used as a string value in an INSERT
** or UPDATE statement.  Use sqlite_decode_binary() to convert the
** string back into its original binary.
**
** The result is written into a preallocated output buffer "out".
** "out" must be able to hold at least (256*n + 1262)/253 bytes.
** In other words, the output will be expanded by as much as 3
** bytes for every 253 bytes of input plus 2 bytes of fixed overhead.
** (This is approximately 2 + 1.019*n or about a 2% size increase.)
*/
static void sqlite_encode_binary(const unsigned char *in, int n, unsigned char *out){
  int i, j, e, m;
  int cnt[256];

  memset(cnt, 0, sizeof(cnt));
  for(i=n-1; i>=0; i--){ cnt[in[i]]++; }
  m = n;
  for(i=1; i<256; i++){
    int sum;
    if( i=='\'' ) continue;
    sum = cnt[i] + cnt[(i+1)&0xff] + cnt[(i+'\'')&0xff];
    if( sum<m ){
      m = sum;
      e = i;
      if( m==0 ) break;
    }
  }
  out[0] = e;
  j = 1;
  for(i=0; i<n; i++){
    int c = (in[i] - e)&0xff;
    if( c==0 ){
      out[j++] = 1;
      out[j++] = 1;
    }else if( c==1 ){
      out[j++] = 1;
      out[j++] = 2;
    }else if( c=='\'' ){
      out[j++] = 1;
      out[j++] = 3;
    }else{
      out[j++] = c;
    }
  }
  out[j++] = 0;
}

/* Return a valid C-string containing the data in the bitvector in ASCII form.
 * This one (unlitke _tostring()) is not human-readable, but is smaller and
 * faster.
 *
 * The returned string must be free()ed later.
 */
char *bitvector_tocstring (bitvector *b)
{
	char *out;

	assert (b != NULL);

	out = malloc ((int)((256*BV_BYTESIZE(b)+1262)/253));
	if (!out) return NULL;
	out[0] = 0;

	sqlite_encode_binary((char *)BV_BITS(b), BV_BYTESIZE(b), out);
	return out;
}

/*
** Decode the string "in" into binary data and write it into "out".
** This routine reverses the encoded created by sqlite_encode_binary().
** The output will always be a few bytes less than the input.  The number
** of bytes of output is returned.  If the input is not a well-formed
** encoding, -1 is returned.
**
** The "in" and "out" parameters may point to the same buffer in order
** to decode a string in place.
*/
static int sqlite_decode_binary(const unsigned char *in, unsigned char *out){
  int i, c, e;
  e = *(in++);
  i = 0;
  while( (c = *(in++))!=0 ){
    if( c==1 ){
      c = *(in++);
      if( c==1 ){
        c = 0;
      }else if( c==2 ){
        c = 1;
      }else if( c==3 ){
        c = '\'';
      }else{
        return -1;
      }
    }
    out[i++] = (c + e)&0xff;
  }
  return i;
}

/* The reverse of the _tocstring(), defined above. The bitvector must be freed
 * later with bitvector_free() */
bitvector *bitvector_fromcstring (char *s)
{
	bitvector *b = bitvector_create (strlen(s)*8);
	if (!b)
		return NULL;

	if (sqlite_decode_binary(s, (char*)BV_BITS(b)) == -1)
		return NULL;

	return b;
}

int bitvector_firstset(bitvector *b)
{
	if (b->dirty)
		b->firstset = bitvector_firstset_find(b, 0);
	return b->firstset;
}

int bitvector_firstunset(bitvector *b)
{
	if (b->dirty)
		b->firstunset = bitvector_firstunset_find(b, 0);
	return b->firstunset;
}
