/*
     This file is part of CPU
     (C) 2003 Blake Matheny (and other contributing authors)

     CPU is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 2, or (at your
     option) any later version.

     CPU is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CPU; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * hasing routines
 * @author Blake Matheny
 * @file hash.c
 **/
#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#ifdef HAVE_CRYPT_H
#include <crypt.h>
#endif
#include "util/hash.h"
#ifdef HAVE_CRACK_H
#include <crack.h>
#endif
#ifndef crypt
extern char *crypt(const char *key, const char *salt);
#endif

char salt[] = "$1$........";
char csalt[] = "........";
const char rstring[] = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQR"
		       "STUVWXYZ~!@#$%^&*()_+{}|:\"<>?`-=[];',./";
const double rlen = 93.00;

const char * hashes[] = {
  "sha1",
  "ssha1",
  "md5",
  "smd5",
  "crypt",
  "clear",
  NULL
};

char *
CPU_getpass ( const char * prompt )
{
  struct termios old, new;
  char * tmp_pass = NULL;
  int i = 0;

  fprintf(stdout, "%s", prompt);
  if ( tcgetattr (fileno (stdin), &old) != 0)
    return NULL;
  new = old;
  new.c_lflag &= ~ECHO;
  if (tcsetattr (fileno (stdin), TCSAFLUSH, &new) != 0)
    return NULL;
  tmp_pass = (char*)malloc(sizeof(char)*PASSWORD_SIZE);
  if ( tmp_pass == NULL )
    return NULL;
  memset(tmp_pass, 0, sizeof(char)*PASSWORD_SIZE);
  if ( fgets(tmp_pass, PASSWORD_SIZE, stdin) == NULL )
    return NULL;
  (void) tcsetattr (fileno (stdin), TCSAFLUSH, &old);
  for ( i = 0; i < (int)strlen(tmp_pass); ++i ) {
    if ( tmp_pass[i] == '\n' ) {
      tmp_pass[i] = '\0';
      break;
    }
  }
  printf("\n");
  return tmp_pass;
}

char *
genPass(int len)
{
  int n = 0;
  int j = 0;
  struct timeval tv;
  char * pass = NULL;

  if ( len < 1 )
    return NULL;

  pass = (char*)malloc(sizeof(char)*len);
  if ( pass == NULL )
    return NULL;
  bzero(pass, (sizeof(char)*len));

  while ( n < len )
    {
      gettimeofday(&tv, NULL);
      srand((unsigned int)tv.tv_usec);
      j = (int)(rlen*rand()/(RAND_MAX+1.0));
      pass[n] = rstring[j];
      n++;
    }
  return pass;
}

hash_t
getHashType(char * hashname)
{
  int i = 0;
  char * temph = NULL;
  hash_t hasht = H_UNKNOWN;

  temph = ctolower(hashname);

  for ( i = 0; hashes[i] != NULL; i++ )
    {
      if ( strcmp(temph, hashes[i]) == 0 )
	{
	  hasht = i;
	  break;
	}
    }
  return hasht;
}

char *
getHash(hash_t hasht, char * password, const char * prefix,
	const char * suffix)
{
  char * temp = NULL;
  char * passphrase = NULL;
  size_t plen = 0;

  if ( password == NULL )
    return NULL;

#ifdef HAVE_LIBCRACK
{
  char * msg = NULL;
  char * newpass = password;
  char * dict = NULL;
  dict = cfg_get_str("GLOBAL", "CRACKLIB_DICTIONARY");
  if ( dict != NULL )
  {
    if ( password[0] != '*' )
      while ( (msg = (char*)FascistCheck(newpass, dict)) != NULL )
      {
	fprintf(stdout, "%s is a bad password: %s\n", newpass, msg);
	newpass = NULL;
	msg = NULL;
	while ( (newpass = CPU_getpass("Enter a new password: ")) == NULL )
	{}
      }
  }
}
#endif

  switch(hasht)
    {
      case H_SHA1:
	temp = sha1_hash(password);
	break;
      case H_SSHA1:
	temp = ssha1_hash(password);
	break;
      case H_MD5:
	temp = md5_hash(password);
	break;
      case H_SMD5:
	temp = smd5_hash(password);
	break;
      case H_CRYPT:
#ifdef HAVE_LIBCRYPT
	temp = crypt(password, cgetSalt());
#else
	fprintf(stderr, "Your c library is missing 'crypt'\n");
#endif
	break;
      case H_CLEAR:
	temp = password;
	break;
      default:
	fprintf(stderr, "getHash: Unknown hash type.\n");
	return NULL;
    }
  if ( temp == NULL )
    return NULL;

  plen = strlen(temp)+1;
  if ( prefix != NULL )
    plen += strlen(prefix);
  if ( suffix != NULL )
    plen += strlen(suffix);

  passphrase = (char*)malloc(sizeof(char)*plen);
  if ( passphrase == NULL )
    return NULL;
  bzero(passphrase, plen*sizeof(char));

  if ( prefix != NULL && suffix != NULL )
    {
      snprintf(passphrase, plen, "%s%s%s", prefix, temp, suffix);
    }
  else if ( prefix != NULL )
    {
      snprintf(passphrase, plen, "%s%s", prefix, temp);
    }
  else if ( suffix != NULL )
    {
      snprintf(passphrase, plen, "%s%s", temp, suffix);
    }
  else
    {
      snprintf(passphrase, plen, "%s", temp);
    }
  return passphrase;
}

char *
sha1_hash(char * password)
{
  unsigned char sha1digest[SHA1_DIGEST_BYTES];
  int b64_len = 0;
  char * b64digest = NULL;

  if ( password == NULL )
    return NULL;

  sha_buffer(password, strlen(password), sha1digest);

  b64_len = CEILING(SHA1_DIGEST_BYTES)*4+1;
  b64digest = (char*)malloc(sizeof(char)*b64_len);
  if ( b64digest == NULL )
    return NULL;
  bzero(b64digest, b64_len);

  if (base64_encode(sha1digest, sizeof(sha1digest), b64digest, b64_len) < 0)
    return NULL;

  return b64digest;
}

char *
ssha1_hash(char * password)
{
  unsigned char sha1digest[SHA1_DIGEST_BYTES];
  int b64_len = 0;
  int slen = 0;
  char * b64digest = NULL;
  char * temp = NULL;
  char * salt = getSalt();

  if ( password == NULL )
    return NULL;

  slen = strlen(password)+SALT_LEN;
  temp = (char*)malloc(slen*sizeof(char));
  if ( temp == NULL )
    return NULL;
  bzero(temp, (slen*sizeof(char)));
  snprintf(temp, slen, "%s%s", password, salt);

  sha_buffer(temp, strlen(temp), sha1digest);

  b64_len = CEILING(SHA1_DIGEST_BYTES+SALT_LEN)*4+1;
  b64digest = (char*)malloc(sizeof(char)*b64_len);
  if ( b64digest == NULL )
    return NULL;
  bzero(b64digest, b64_len);

  Free(temp);
  temp = (char *)malloc((sizeof(sha1digest)+SALT_LEN)*sizeof(char));
  if ( temp == NULL )
    return NULL;
  bzero(temp, ((sizeof(sha1digest)+SALT_LEN)*sizeof(char)));
  snprintf(temp, (sizeof(sha1digest)+SALT_LEN), "%s%s", sha1digest, salt);

  if (base64_encode(temp, sizeof(sha1digest)+SALT_LEN, b64digest, b64_len) < 0)
    return NULL;

  return b64digest;
}

char *
md5_hash(char * password)
{
  unsigned char md5digest[MD5_DIGEST_BYTES];
  int b64_len = 0;
  char * b64digest = NULL;

  if ( password == NULL )
    return NULL;

  md5_buffer(password, strlen(password), md5digest);

  b64_len = CEILING(MD5_DIGEST_BYTES)*4+1;
  b64digest = (char*)malloc(sizeof(char)*b64_len);
  if ( b64digest == NULL )
    return NULL;
  bzero(b64digest, b64_len);

  if (base64_encode(md5digest, sizeof(md5digest), b64digest, b64_len) < 0)
    return NULL;

  return b64digest;
}

char *
smd5_hash(char * password)
{
  unsigned char md5digest[MD5_DIGEST_BYTES];
  int b64_len = 0;
  int slen = 0;
  char * b64digest = NULL;
  char * temp = NULL;
  char * salt = getSalt();

  if ( password == NULL )
    return NULL;

  slen = strlen(password)+SALT_LEN;
  temp = (char*)malloc(slen*sizeof(char));
  if ( temp == NULL )
    return NULL;
  bzero(temp, (slen*sizeof(char)));
  snprintf(temp, slen, "%s%s", password, salt);

  md5_buffer(temp, strlen(temp), md5digest);

  b64_len = CEILING(MD5_DIGEST_BYTES+SALT_LEN)*4+1;
  b64digest = (char*)malloc(sizeof(char)*b64_len);
  if ( b64digest == NULL )
    return NULL;
  bzero(b64digest, b64_len);

  Free(temp);
  temp = (char *)malloc((sizeof(md5digest)+SALT_LEN)*sizeof(char));
  if ( temp == NULL )
    return NULL;
  bzero(temp, ((sizeof(md5digest)+SALT_LEN)*sizeof(char)));
  snprintf(temp, (sizeof(md5digest)+SALT_LEN), "%s%s", md5digest, salt);

  if (base64_encode(temp, sizeof(md5digest)+SALT_LEN, b64digest, b64_len) < 0)
    return NULL;

  return b64digest;
}

int
cRandom(int minr, int maxr)
{
  int j = 0;
  int t = 0;
  struct timeval tv;
  if ( gettimeofday(&tv, NULL) < 0 )
    return -1;

  srand((unsigned int)tv.tv_usec);

  maxr = ABS(maxr);
  minr = ABS(minr);

  if ( minr > maxr )
    {
      t = maxr;
      maxr = minr;
      minr = t;
    }
  j = minr+(int)((double)((maxr-minr)+1) * rand()/(RAND_MAX+1.0));
  return j;
}

char *
getSalt ()
{
  unsigned long seed[2];
  const char *const seedchars =
    "./0123456789ABCDEFGHIJKLMNOPQRST" "UVWXYZabcdefghijklmnopqrstuvwxyz";
  int i;

  seed[0] = time (NULL);
  seed[1] = getpid () ^ (seed[0] >> 14 & 0x30000);

  for (i = 3; i < 8; i++)
    salt[i] = seedchars[(seed[i / 5] >> (i % 5) * 6) & 0x3f];
  return salt;
}

char *
cgetSalt ()
{
  unsigned long seed[2];
  const char *const seedchars =
    "./0123456789ABCDEFGHIJKLMNOPQRST" "UVWXYZabcdefghijklmnopqrstuvwxyz";
  int i;

  seed[0] = time (NULL);
  seed[1] = getpid () ^ (seed[0] >> 14 & 0x30000);

  for (i = 0; i < 8; i++)
    csalt[i] = seedchars[(seed[i / 5] >> (i % 5) * 6) & 0x3f];
  return csalt;
}
