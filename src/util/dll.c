/*
     This file is part of CPU
     (C) 2003 Blake Matheny (and other contributing authors)

     CPU is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 2, or (at your
     option) any later version.

     CPU is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CPU; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * Routines for dynamically loadable libraries
 * @author Blake Matheny
 * @file dll.c
 **/
#include <stdio.h>
#include <dlfcn.h>
#include <stdlib.h>
#include <string.h>
#include "util/dll.h"

char *
getLibName(char * mstring)
{
  char * temp = NULL;
  int len = 0;

  if ( mstring == NULL )
    {
      fprintf(stderr, "getLibName: mstring is null.\n");
      return NULL;
    }

  len = strlen(mstring)+strlen(LIB_PREFIX)+strlen(LIB_SUFFIX)+1;
  temp = (char*)malloc(sizeof(char)*len);
  if ( temp == NULL )
    {
      perror("getLibName: malloc");
      return NULL;
    }
  bzero(temp, len);

  snprintf(temp, len, "%s%s%s", LIB_PREFIX, mstring, LIB_SUFFIX);
  return temp;
}

CPU_Method *
CPU_loadLibrary(char * mstring)
{
  char * libname = NULL;
  void * handle = NULL;
  CPU_Method * temp = NULL;
  CPU_method mtemp;

  if ( mstring == NULL )
    {
      fprintf(stderr, "CPU_loadLibrary: mstring is null.\n");
      return NULL;
    }

  libname = getLibName(mstring);
  if ( libname == NULL )
    {
      fprintf(stderr, "CPU_loadLibrary: libname is null.\n");
      return NULL;
    }

  handle = dlopen(libname, RTLD_NOW);
  if ( handle == NULL )
    {
      fprintf(stderr, "CPU_loadLibrary: dlopen(%s, RTLD_NOW) failed.\n",
		      libname);
      fprintf(stderr, "CPU_loadLibrary: %s\n", dlerror());
      return NULL;
    }

  mtemp.initialize = (CPU_methodInit)dlsym(handle, "CPU_init");
  if ( mtemp.initialize == NULL )
    {
      fprintf(stderr, "CPU_loadLibrary: Resolving method 'CPU_init' in "\
		      "library '%s' failed.\n", libname);
      return NULL;
    }

  temp = (CPU_Method*)malloc(sizeof(CPU_Method));
  if ( temp == NULL )
    {
      fprintf(stderr, "CPU_loadLibrary: temp is null.\n");
      return NULL;
    }
  bzero(temp, sizeof(CPU_Method));

  temp->libraryHandle = handle;
  temp->libname = libname;
  temp->method = mtemp;

  return temp;
}

int
CPU_unloadLibrary(CPU_Method * cmethod)
{
  if ( cmethod == NULL )
    return -1;

  if ( cmethod->libraryHandle == NULL )
    return -1;

  if ( dlclose(cmethod->libraryHandle) != 0 )
    {
      fprintf(stderr, "CPU_unloadLibrary: dlclose(%s) failed.\n",
		      cmethod->libname);
      fprintf(stderr, "CPU_unloadLibrary: %s\n", dlerror());
      return -1;
    }
  return 0;
}
