/*
     This file is part of CPU
     (C) 2003 Blake Matheny (and other contributing authors)

     CPU is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 2, or (at your
     option) any later version.

     CPU is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CPU; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * Base64 encoding routines from GNUnet
 * @author Christian Grothoff <grothoff@cs.purdue.edu>
 * @file base64.c
 **/
#include <stdio.h>
#include <stdlib.h>

#define MAX_CHAR_PER_LINE 76
#define FILLCHAR '='
static char * cvt = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"\
                    "abcdefghijklmnopqrstuvwxyz"\
                    "0123456789+/";

/**
 * Encode into Base64.
 *
 * @param src the src to encode
 * @param srclen the srclength of the input
 * @param target where to write the target (*target should be NULL,
 *   is allocated)
 * @return the size of the target
 **/
unsigned int
base64_encode(char * src, size_t srclen, char * target, size_t targetsize)
{
  unsigned int i;
  char c;
  unsigned int ret;

  ret = 0;
  for (i = 0; i < srclen; ++i)
    {
      if ( ret + 3 > targetsize )
	return -1;

      c = (src[i] >> 2) & 0x3f;
      target[ret++] = cvt[(int)c];
      c = (src[i] << 4) & 0x3f;

      if (++i < srclen)
	c |= (src[i] >> 4) & 0x0f;

      target[ret++] = cvt[(int)c];

      if (i < srclen)
	{
	  c = (src[i] << 2) & 0x3f;

	  if (++i < srclen)
	    c |= (src[i] >> 6) & 0x03;

	  target[ret++] = cvt[(int)c];
	}
      else
	{
	  ++i;
	  target[ret++] = FILLCHAR;
	}
      if (i < srclen)
	{
	  c = src[i] & 0x3f;
	  target[ret++] = cvt[(int)c];
	}
      else
	{
	  target[ret++] = FILLCHAR;
	}
    }

  return ret;
}
