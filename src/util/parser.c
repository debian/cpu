/*
     This file is part of CPU
     (C) 2003 Blake Matheny (and other contributing authors)

     CPU is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 2, or (at your
     option) any later version.

     CPU is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CPU; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * Generic parser routines. Has features different from config file parser
 * @author Blake Matheny
 * @file parser.c
 **/

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "util/parser.h"
#include "util/helper.h"

Parser * __parse(Parser * parse, char * text, const char * delim,
	         const char * ignore);
char * delWhite(char ** string);

Parser *
parseInit(void)
{
  Parser * new;
  new = (Parser*)malloc(sizeof(Parser));
  if ( new == NULL )
    return NULL;
  bzero(new, sizeof(Parser));
  new->next = NULL;
  return new;
}

Parser *
parseFile(Parser * parse, const char * filename, const char * delim,
	  const char * ignore)
{
  int fd = 0;
  ssize_t retval = 0;
  void * temp;
  struct stat buf;

  if ( parse == NULL || filename == NULL || delim == NULL )
    return NULL;

  fd = open(filename, O_RDONLY);
  if ( fd < 0 )
    {
      perror(filename);
      return NULL;
    }
  if ( stat(filename, &buf) < 0 )
    {
      perror(filename);
      return NULL;
    }

  temp = malloc(buf.st_size);
  if ( temp == NULL )
    return NULL;
  bzero(temp, buf.st_size);

  retval = read(fd, temp, buf.st_size);
  close(fd);
  if ( retval != buf.st_size )
    printf("Hmm, didn't read all wanted data. Continuing\n");
  if ( retval < 0 )
    return NULL;
  if ( temp == NULL )
    return NULL;
  parse = __parse(parse, temp, delim, ignore);

  return parse;
}

Parser *
__parse(Parser * parse, char * text, const char * delim,
	const char * ignore)
{
  char ** car;
  char ** cart;
  int newlines = 0;
  int i, j = 0;
  int longestline = 0;
  int counter = 0;
  Parser * p;
  Parser * pos;

  /* find longest line and number of lines for memory allocation */
  for ( i = 0; i < (int)strlen(text); i++ )
    {
      counter++;
      if ( text[i] == '\n' )
	{
	  newlines++;
	  if ( counter > longestline )
	      longestline = counter;
	  counter = 0;
	}
    }

  car = (char**)malloc(sizeof(char*)*newlines);
  if ( car == NULL )
    return NULL;
  bzero(car, sizeof(char*)*newlines);

  for ( i = 0; i < newlines; i++ )
    {
      car[i] = (char*)malloc(sizeof(char)*longestline);
      if ( car[i] == NULL )
	return NULL;
      bzero(car[i], sizeof(char)*longestline);
    }

  /* assign every line terminated by a \n to an array elemnt */
  for ( i = 0; i < newlines; i++ )
    car[i] = strdup(getToken(&text, "\n"));

  /* remove all lines that start with 'ignore' */
  if ( ignore != NULL )
    {
      int len = strlen(ignore);
      int j = 0;
      for ( i = 0; i < newlines; i++ )
	{
	  counter = 0;
	  for ( j = 0; j < len; j++ )
	    {
	      if ( car[i][j] == ignore[j] )
		counter++;
	    }
	  if ( counter == len )
	    {
	      free(car[i]);
	      car[i] = NULL;
	    }
	}
    }
  /* remove all blank lines */
  for ( i = 0; i < newlines; i++ )
    {
      if ( car[i] != NULL && car[i][0] == 0 )
	{
	  free(car[i]);
	  car[i] = NULL;
	}
    }

  counter = newlines;
  newlines = 0;
  for ( i = 0; i < counter; i++ )
    {
      if ( car[i] != NULL )
	newlines++;
    }
  cart = (char**)malloc(sizeof(char*)*newlines);
  if ( cart == NULL )
    return NULL;
  bzero(cart, sizeof(char*)*newlines);
  /* resize array to only hold good elements */
  for ( i = j = 0; i < counter; i++ )
    {
      if ( car[i] != NULL )
	{
	  cart[j] = car[i];
	  j++;
	}
    }

  /* now actually start parsing */
  for ( i = 0; i < newlines; i++ )
    {
      char * token = NULL;
      if ( cart[i][0] == ' ' ) /* we're continuing a line */
	{
	  char * m = NULL;
	  char * temp = NULL;
	  int tlen = 0;
	  pos = parse;
	  if ( pos == NULL )
	    {
	      printf("%d: Malformed file, cannot continue parsing.\n",
		     __LINE__);
	      return NULL;
	    }
	  while ( pos->next != NULL )
	    {
	      pos = pos->next;
	    }
	  if ( pos->cont == false )
	    {
	      printf("%d: Malformed file, cannot continue parsing.\n",
		     __LINE__);
	      return NULL;
	    }
	  if ( pos->attrval == NULL )
	    {
	      printf("%d: Error, cannot continue parsing.\n", __LINE__);
	      return NULL;
	    }
	  tlen = strlen(pos->attrval) + strlen(cart[i]) + 1;
	  m = (char*)malloc(sizeof(char)*tlen);
	  if ( m == NULL )
	    {
	      printf("%d: Out of memory, cannot continue parsing.\n", __LINE__);
	      return NULL;
	    }
	  bzero(m, tlen);
	  strncat(m, pos->attrval, strlen(pos->attrval));
	  temp = getToken(&cart[i], " ");
	  if ( temp == NULL || temp[0] == 0 )
	    strncat(m, cart[i], tlen);
	  else
	    strncat(m, temp, tlen);
	  pos->attrval = strdup(m);
	}
      else
	{
	  p = NULL;
	  p = (Parser*)malloc(sizeof(Parser));
	  if ( p == NULL )
	    return NULL;
	  bzero(p, sizeof(Parser));
	  p->next = NULL;
	  p->cont = false;

	  if ( strstr(cart[i], delim) == NULL )
	    {
	      printf("%d: Malformed file, cannot continue parsing.\n",
		     __LINE__);
	      return NULL;
	    }
	  token = getToken(&cart[i], delim);
	  if ( token == NULL )
	    {
	      printf("%d: Malformed file, cannot continue parsing.\n",
		     __LINE__);
	      return NULL;
	    }
	  p->attr = strdup(token);
	  token = NULL;
	  token = getToken(&cart[i], delim);
	  if ( token == NULL )
	    {
	      printf("%d: Malformed file, cannot continue parsing.\n",
	             __LINE__);
	      return NULL;
	    }
	  if ( token[0] == 0 )
	    {
	      p->cont = true;
	      token = getToken(&cart[i], delim);
	      if ( token == NULL )
		{
		  printf("%d: Malformed file, cannot continue parsing.\n",
			 __LINE__);
		  return NULL;
		}
	    }
	  p->attrval = delWhite(&token);
	  pos = parse;
	  if ( i )
	    {
	      while(pos->next != NULL )
		{
		  pos = pos->next;
		}
	      pos->next = p;
	    }
	  else
	    {
	      parse->attr = p->attr;
	      parse->attrval = p->attrval;
	      parse->cont = p->cont;
	      parse->next = p->next;
	    }
	}
    }
  return parse;
}

char *
delWhite(char ** string)
{
  while(isspace((int)**string))
    {
      (*string)++;
    }
  return *string;
}

char *
parseGetStr(Parser * parse, const char * attr)
{
  Parser * pos;
  pos = parse;
  while(pos != NULL)
    {
      if ( strcmp(pos->attr, attr) == 0 )
	return pos->attrval;
      pos = pos->next;
    }
  return NULL;
}

int
parseGetInt(Parser * parse, const char * attr)
{
  Parser * pos;
  pos = parse;
  while(pos != NULL)
    {
      if ( strcmp(pos->attr, attr) == 0 )
	return atoi(pos->attrval);
      pos = pos->next;
    }
  return -1;
}

long
parseGetLong(Parser * parse, const char * attr)
{
  Parser * pos;
  pos = parse;
  while(pos != NULL)
    {
      if ( strcmp(pos->attr, attr) == 0 )
	return atol(pos->attrval);
      pos = pos->next;
    }
  return -1;
}

float
parseGetFloat(Parser * parse, const char * attr)
{
  Parser * pos;
  pos = parse;
  while(pos != NULL)
    {
      if ( strcmp(pos->attr, attr) == 0 )
	return atof(pos->attrval);
      pos = pos->next;
    }
  return -1;
}

void
parseDestroy(Parser * parse)
{
  Parser * pos;
  Parser * pos2;
  pos = parse;
  while ( pos != NULL )
    {
      free(pos->attr);
      free(pos->attrval);
      pos2 = pos;
      pos = pos->next;
      free(pos2);
    }
  return;
}
