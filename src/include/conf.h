/*
     This file is part of CPU
     (C) 2003 Blake Matheny (and other contributing authors)

     CPU is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 2, or (at your
     option) any later version.

     CPU is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CPU; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * general configuration stuff. every file should include this.
 * @author Blake Matheny
 * @file conf.h
 **/

#ifndef CONF_H
#define CONF_H

#ifdef  __cplusplus
extern "C" {
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif


#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

#if TIME_WITH_SYS_TIME
# include <sys/time.h>
# include <time.h>
#else
# if HAVE_SYS_TIME_H
#  include <sys/time.h>
# else
#  include <time.h>
# endif
#endif

#ifdef HAVE_STRINGS_H
#include <strings.h>
#endif

#include "debugging.h"
#include "util/cputil.h"

/* use "c:hm:V" with getopt_long */
/* config and method are reserved, along with their short options */
#define DEFAULT_OPTIONS \
    { "config", 1, 0, 'C' }, \
    { "help", 0, 0, 'h' }, \
    { "method", 1, 0, 'M' }, \
    { "verbose", 0, 0, 'v' }, \
    { "version", 0, 0, 'V' }

typedef int bool;
#define true 1
#define false 0

#ifdef DEBUG
#define CTEST() fprintf(stderr, "%s: %d\n", __FILE__, __LINE__)
#else
#define CTEST() {}
#endif

#ifdef  __cplusplus
}
#endif

#endif
/* end of conf.h */
