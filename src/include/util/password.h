/*
     This file is part of CPU
     (C) 2003 Blake Matheny (and other contributing authors)

     CPU is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 2, or (at your
     option) any later version.

     CPU is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CPU; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * Password File Helpers
 * @author Blake Matheny
 * @file password.h
 **/

#ifndef PASSWORD_H
#define PASSWORD_H

#ifdef  __cplusplus
extern "C" {
#endif

/* also used for LDAP
 * pw_name -> uid
 * pw_passwd -> userPassword
 * pw_uid -> uidNumber
 * pw_gid -> gidNumber
 * pw_gecos -> gecos
 * pw_dir -> homeDirectory
 * pw_shell -> loginShell
 * sp_lstchg -> shadowLastChange
 * sp_min -> shadowMin
 * sp_max -> shadowMax
 * sp_warn -> shadowWarning
 * sp_inact -> shadowInactive
 * sp_expire -> shadowExpire
 * sp_flag -> shadowFlag
 */
struct cpass {
  char * pw_name;
  char * pw_passwd;
  uid_t pw_uid;
  gid_t pw_gid;
  char * pw_gecos;
  char * pw_dir;
  char * pw_shell;
  long sp_lstchg;
  int sp_min;
  int sp_max;
  int sp_warn;
  int sp_inact;
  int sp_expire;
  int sp_flag;
};

typedef enum {
  PASSWORD,
  SHADOW,
} pfile_t;

struct cpass * cgetpwent(char * filename, char * name, pfile_t ftype);

#ifdef  __cplusplus
}
#endif

#endif
/* end of password.h */
