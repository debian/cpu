/*
     This file is part of CPU
     (C) 2003 Blake Matheny (and other contributing authors)

     CPU is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 2, or (at your
     option) any later version.

     CPU is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CPU; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * define some hashing types and prototypes
 * @author Blake Matheny
 * @file hash.h
 **/

#ifndef HASH_H
#define HASH_H

#ifdef  __cplusplus
extern "C" {
#endif

#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#ifdef HAVE_CRYPT_H
#include <crypt.h>
#endif
#include "conf.h"
#include "cmd5.h"
#include "csha1.h"

#define CEILING(x)      ((double)x > (int)x ? (int)x + 1 : (int)x)
#define MD5_DIGEST_BYTES 16
#define SHA1_DIGEST_BYTES 20
#define SALT_LEN 11
#define ABS(x) (x < 0 ? x*-1:x)
#define PASSWORD_SIZE 128
  
/* hash_t should have a one-to-one correspondence with hashes */
typedef enum {
  H_SHA1 = 0,
  H_SSHA1,
  H_MD5,
  H_SMD5,
  H_CRYPT,
  H_CLEAR,
  H_UNKNOWN,
} hash_t;

char * CPU_getpass ( const char * prompt );
unsigned int base64_encode(char * src, size_t srclen, char * target,
			   size_t targetsize);
char * getHash(hash_t hasht, char * password, const char * prefix,
	       const char * suffix);
char * genPass(int len);
hash_t getHashType(char * hashname);
char * getSalt();
char * cgetSalt();
char * sha1_hash(char * password);
char * ssha1_hash(char * password);
char * md5_hash(char * password);
char * smd5_hash(char * password);
int cRandom(int minr, int maxr);

#ifdef  __cplusplus
}
#endif

#endif
/* end of hash.h */
