#ifndef __BITVECTOR_H
#define __BITVECTOR_H

#ifdef __cplusplus
extern "C" {
#endif

#include "config.h"

#ifdef HAVE_STDINT_H
#include <stdint.h>
#else
#include <inttypes.h>
#endif

#ifndef true
#define true 1
#define false 0
#endif

/* It is better to not change this structure. It is not guarenteed to be
 * stable. Use the provided functions and macros. */
struct bitvector_s
{
	/* Pointer to an array of uints: the actual bits are stored here */
	uint32_t *bits;

	/* The number of bits in the array. This is always rounded up to chunk
	 * boundries */
	int size;

	/* The number of elements in the array */
	int arraylen;

	/* A cached index: the first bit set. This is valid if dirty is false
	 */
	int firstset;

	/* A cached index: the first bit unset. This is valid if dirty is
	   false */
	int firstunset;

	/* True if the firstset/firstunset variables may have been changed and
	   need to be recalculated */
	int dirty;
};

typedef struct bitvector_s bitvector;

/* The number of bytes of each individual element in the array that stores the
 * bits */
#define BV_CHUNKSIZE_BYTES		(sizeof(uint32_t))
#define BV_CHUNKSIZE			(BV_CHUNKSIZE_BYTES<<3)

/* The number of bits availible for use in the bitvector a */
#define BV_BITSIZE(a)			(a->size)
/* The number of bytes availible for use in the bitvector a */
#define BV_BYTESIZE(a)			(a->size>>3)

/* Retrieve a pointer to the array of bits */
#define BV_BITS(a)				(a->bits)

/* Random convienence macros that make life easier */
#ifndef min
#  define min(a,b)				(a<b?a:b)
#endif

#ifndef max
#  define max(a,b)				(a>b?a:b)
#endif

/* Return a pointer to a new bitvector of size s, or return NULL on failure */
bitvector *bitvector_create (int s);
/* Same as above, but do not initialize the bitvector data to all zeros */
bitvector *bitvector_create_dirty (int size);
/* Frees the bitvector b */
void bitvector_free (bitvector *b);

/* Sets bit #n to true, in the vector b */
void bitvector_set (bitvector *b, unsigned int n);
/* Sets bit #n to false, in the vector b */
void bitvector_unset (bitvector *b, unsigned int n);
/* Returns 0 if bit #n is unset in the vector b, and nonzero otherwise */
int bitvector_get (bitvector *b, unsigned int n);

/* Stores a string ("10110011"-style) representation of the bitvector b, and
 * stores it in the provided buffer. It is the users responsibility to provide
 * a buffer of sufficient size, which is: BV_BITSIZE(a)+1. The +1 is because it
 * is null-terminated. */
void bitvector_tostring (bitvector *b, char *buffer);
/* The above, but in reverse */
bitvector *bitvector_fromstring (char *s);
/* Read all the bits of an integer into a new bitvector */
bitvector *bitvector_fromint (int n);

/* Convert it to a string which is has no NULLs in it, and therefore is a valid
 * C-string (good for storing in databases, etc */
char *bitvector_tocstring (bitvector *b);
bitvector *bitvector_fromcstring (char *s);

/* Resize the bitvector b to be able to store n bytes. The requested size is
 * rounded up to the nearest chunk size (see above, by the bitvector_s
 * definition, for a discussion of chunk). The bitvector is either zero-padded
 * or truncated to accomadate the new length. The _ns function does not copy
 * the old data. No initialization is performed on it.
 *
 * Zero (0) is returned on success, and non-zero on failure. */

int bitvector_resize (bitvector *b, unsigned int n);
int bitvector_resize_ns (bitvector *b, unsigned int n);

/* In _copy, the user supplies a bitvector struct, already allocated. In
 * _duplicate(), a new one is returned, and it is the users responsibility to
 * bitvector_free() it. */
int bitvector_copy (bitvector *src, bitvector *dst);
bitvector *bitvector_duplicate (bitvector *b);

/* dest = lhs & rhs */
int bitvector_and (bitvector *dest, bitvector *lhs, bitvector *rhs);
/* lhs &= rhs */
void bitvector_andeq (bitvector *lhs, bitvector *rhs);
/* dest = lhs | rhs */
int bitvector_or (bitvector *dest, bitvector *lhs, bitvector *rhs);
/* lhs |= rhs */
int bitvector_oreq (bitvector *lhs, bitvector *rhs);
/* dest = lhs ^ rhs */
int bitvector_xor (bitvector *dest, bitvector *lhs, bitvector *rhs);
/* lhs ^= rhs */
int bitvector_xoreq (bitvector *lhs, bitvector *rhs);
/* nb = ~b. This is more efficient than _copy() and _invert() */
int bitvector_not (bitvector *nb, bitvector *b);
/* b = ~b */
void bitvector_invert (bitvector *b);

/* Returns zero if b has any bets set to on or true. Returns nonzero otherwise
 * (Currently 1, but this may change.). */
int bitvector_isempty (bitvector *b);
int bitvector_isfull (bitvector *b);

/* Shift b left n bits. Left shifting is never destructive. Right shifted data
 * is lost if it passes beyond the decimal. */
void bitvector_leftshift (bitvector *b, int n);
void bitvector_rightshift (bitvector *b, int n);

/* Clears all bits. Does not resize */
void bitvector_clear (bitvector *b);

/* Equality check */
int bitvector_isequal (bitvector *a, bitvector *b);

/* Returns the index of the first set bit, or -1 if none */
int bitvector_firstset (bitvector *b);
/* Returns the index of the first unset bit, or -1 if none */
int bitvector_firstunset (bitvector *b);

#ifdef __cplusplus
}
#endif

#endif
