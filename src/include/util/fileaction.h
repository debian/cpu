/*
     This file is part of CPU
     (C) 2003 Blake Matheny (and other contributing authors)

     CPU is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 2, or (at your
     option) any later version.

     CPU is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CPU; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * function prototypes
 * @author Blake Matheny
 * @file fileaction.h
 **/

#ifndef FILEACTION_H
#define FILEACTION_H

#ifdef  __cplusplus
extern "C" {
#endif

char * getHomeDir(char * prefix, char * suffix);
int atoo(char *s);
int remdir(char * directory);
int copy(char * directory, char * newdirectory, uid_t pw_uid, gid_t pw_gid);

#ifdef  __cplusplus
}
#endif

#endif
/* end of fileaction.h */
