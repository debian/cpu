/*
     This file is part of CPU>
     (C) 2003 Blake Matheny (and other contributing authors)

     CPU> is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 2, or (at your
     option) any later version.

     CPU> is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CPU>; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * just include all the util headers, plus some macros
 * @author Blake Matheny
 * @file cpuutil.h
 **/

#ifndef CPUTIL_H
#define CPUTIL_H

#ifdef  __cplusplus
extern "C" {
#endif

#ifndef HAVE_BZERO
#define bzero(x,y) memset(x,0,y)
#endif

#include "cgetopt.h"
#include "helper.h"
#include "parseconfig.h"
#include "dll.h"
#include "xmem.h"
#include "hash.h"
#include "password.h"
#include "fileaction.h"

#ifdef  __cplusplus
}
#endif

#endif
/* end of cputil.h */
