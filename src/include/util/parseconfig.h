/*
     This file is part of CPU

     CPU is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 2, or (at your
     option) any later version.

     CPU is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CPU; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/


/**
 * @file util/parseconfig.h
 * @author Gerd Knorr <kraxel@bytesex.org>
 *
 * Configuration file parsing, taken from
 * xawtv (GPL).
 **/

#ifndef PARSECONFIG_H
#define PARSECONFIG_H

int    cfg_parse_file(char *filename);
void   cfg_parse_option(char *section, char *tag, char *value);
void   cfg_parse_options(int *argc, char **argv);

char** cfg_list_sections(void);
char** cfg_list_entries(char *name);
char*  cfg_get_str(char *sec, char *ent);
int    cfg_get_int(char *sec, char *ent);
int    cfg_get_signed_int(char *sec, char *ent);
float  cfg_get_float(char *sec, char *ent);
long   cfg_get_long(char *sec, char *ent);

#endif
