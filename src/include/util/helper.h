/*
     This file is part of CPU
     (C) 2003 Blake Matheny (and other contributing authors)

     CPU is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 2, or (at your
     option) any later version.

     CPU is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CPU; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * help prototypes
 * @author Blake Matheny
 * @file helper.h
 **/

#ifndef HELPER_H
#define HELPER_H

#ifdef  __cplusplus
extern "C" {
#endif

char * getToken(char **str, const char * delims);
char * ctolower(char * str);

void printDefaultHelp(void);
void printDefaultVersion(void);

#ifdef  __cplusplus
}
#endif

#endif
/* end of helper.h */
