/*
     This file is part of CPU
     (C) 2003 Blake Matheny (and other contributing authors)

     CPU is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 2, or (at your
     option) any later version.

     CPU is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CPU; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * header file for dynamically loadable libraries
 * @author Blake Matheny
 * @file dll.h
 **/

#ifndef DLL_H
#define DLL_H

#ifdef  __cplusplus
extern "C" {
#endif

#include "conf.h"

#define LIB_PREFIX "libcpu_"
#define LIB_SUFFIX ".so"

typedef int (*CPU_methodInit)(int argc, char *argv[]);

typedef struct CPU_method {
  CPU_methodInit initialize;
} CPU_method;

typedef struct CPU_Method {
  void * libraryHandle;
  const char * libname;
  CPU_method method;
} CPU_Method;

CPU_Method * CPU_loadLibrary(char * mstring);
int CPU_unloadLibrary(CPU_Method * cmethod);
char * getLibName(char * mstring);

#ifdef  __cplusplus
}
#endif

#endif
/* end of dll.h */
