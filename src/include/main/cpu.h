/*
     This file is part of CPU
     (C) 2003 Blake Matheny (and other contributing authors)

     CPU is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 2, or (at your
     option) any later version.

     CPU is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CPU; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * Header file for cpu.c
 * @author Blake Matheny
 * @file cpu.h
 **/

#ifndef CPU_H
#define CPU_H

#ifdef  __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <unistd.h>
#include "conf.h"

int parseCommand(int argc, char *argv[]);

#define CPU_OPTS \
    {"a", 2, 0, 'a'}, \
    {"b", 2, 0, 'b'}, \
    {"c", 2, 0, 'c'}, \
    {"d", 2, 0, 'd'}, \
    {"e", 2, 0, 'e'}, \
    {"f", 2, 0, 'f'}, \
    {"g", 2, 0, 'g'}, \
    {"i", 2, 0, 'i'}, \
    {"j", 2, 0, 'j'}, \
    {"k", 2, 0, 'k'}, \
    {"l", 2, 0, 'l'}, \
    {"m", 2, 0, 'm'}, \
    {"n", 2, 0, 'n'}, \
    {"o", 2, 0, 'o'}, \
    {"p", 2, 0, 'p'}, \
    {"q", 2, 0, 'q'}, \
    {"r", 2, 0, 'r'}, \
    {"s", 2, 0, 's'}, \
    {"t", 2, 0, 't'}, \
    {"u", 2, 0, 'u'}, \
    {"v", 2, 0, 'v'}, \
    {"w", 2, 0, 'w'}, \
    {"x", 2, 0, 'x'}, \
    {"y", 2, 0, 'y'}, \
    {"z", 2, 0, 'z'}, \
    {"A", 2, 0, 'A'}, \
    {"B", 2, 0, 'B'}, \
    {"D", 2, 0, 'D'}, \
    {"E", 2, 0, 'E'}, \
    {"F", 2, 0, 'F'}, \
    {"G", 2, 0, 'G'}, \
    {"H", 2, 0, 'H'}, \
    {"I", 2, 0, 'I'}, \
    {"J", 2, 0, 'J'}, \
    {"K", 2, 0, 'K'}, \
    {"L", 2, 0, 'L'}, \
    {"N", 2, 0, 'N'}, \
    {"O", 2, 0, 'O'}, \
    {"P", 2, 0, 'P'}, \
    {"Q", 2, 0, 'Q'}, \
    {"R", 2, 0, 'R'}, \
    {"S", 2, 0, 'S'}, \
    {"T", 2, 0, 'T'}, \
    {"U", 2, 0, 'U'}, \
    {"W", 2, 0, 'W'}, \
    {"X", 2, 0, 'X'}, \
    {"Y", 2, 0, 'Y'}, \
    {"Z", 2, 0, 'Z'}, \
    {"0", 2, 0, '0'}, \
    {"1", 2, 0, '1'}, \
    {"2", 2, 0, '2'}, \
    {"3", 2, 0, '3'}, \
    {"4", 2, 0, '4'}, \
    {"5", 2, 0, '5'}, \
    {"6", 2, 0, '6'}, \
    {"7", 2, 0, '7'}, \
    {"8", 2, 0, '8'}, \
    {"9", 2, 0, '9'}, \
    {"addfile", 2, 0, 'a'}, \
    {"cn", 2, 0, 'A'}, \
    {"userbase", 2, 0, 'b'}, \
    {"groupbase", 2, 0, 'B'}, \
    {"gecos", 2, 0, 'c'}, \
    {"directory", 2, 0, 'd'}, \
    {"binddn", 2, 0, 'D'}, \
    {"email", 2, 0, 'e'}, \
    {"lastname", 2, 0, 'E'}, \
    {"firstname", 2, 0, 'f'}, \
    {"passfile", 2, 0, 'F'}, \
    {"gid", 2, 0, 'g'}, \
    {"sgroups", 2, 0, 'G'}, \
    {"hash", 2, 0, 'H'}, \
    {"skel", 2, 0, 'k'}, \
    {"newusername", 2, 0, 'l'}, \
    {"lock", 2, 0, 'L'}, \
    {"makehome", 2, 0, 'm'}, \
    {"newgroupname", 2, 0, 'n'}, \
    {"hostname", 2, 0, 'N'}, \
    {"nonposix", 2, 0, 'o'}, \
    {"password", 2, 0, 'p'}, \
    {"port", 2, 0, 'P'}, \
    {"removehome", 2, 0, 'r'}, \
    {"random", 2, 0, 'R'}, \
    {"shell", 2, 0, 's'}, \
    {"shadfile", 2, 0, 'S'}, \
    {"timeout", 2, 0, 't'}, \
    {"uid", 2, 0, 'u'}, \
    {"unlock", 2, 0, 'U'}, \
    {"bindpass", 2, 0, 'w'}, \
    {"tls", 2, 0, 'x'}, \
    {"exec", 2, 0, 'X'}, \
    {"yes", 2, 0, 'y'}, \
    {"uri", 2, 0, 'Z'}

#ifdef  __cplusplus
}
#endif

#endif
/* end of cpu.h */
