/*
     This file is part of CPU
     (C) 2003 Blake Matheny (and other contributing authors)

     CPU is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 2, or (at your
     option) any later version.

     CPU is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CPU; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * stuff for password module
 * @author Blake Matheny
 * @file passwd.h
 **/

#ifndef PASSWD_H
#define PASSWD_H

#ifdef  __cplusplus
extern "C" {
#endif

#include "conf.h"

#define __VERSION "0.0.1"

typedef enum {
  USERADD = 0,
  USERMOD,
  USERDEL,
  GROUPADD,
  GROUPMOD,
  GROUPDEL,
} passop_t;

typedef struct Passwd {
  passop_t op;
  bool lock;
  bool unlock;
  bool md;	  /* make directory */
  bool rmd;	  /* remove directory */
  char ** groups; /* groups[0] is initial group, >0 are others */
  char * skeldir;
  char * newname;
  struct cpass * p;
} Passwd;

int parseCommand(int argc, char * argv[], Passwd * pt);
void printHelp(passop_t op);
void printVersion(void);
Passwd * initPass(void);
int populatePass(Passwd * pt);
int passwdOperation(Passwd * pt);

void printUserHelp(passop_t op);
void printGroupHelp(passop_t op);

uid_t getNextUid(void);
gid_t getNextGid(void);
#ifdef  __cplusplus
}
#endif

#endif
/* end of passwd.h */
