/*
     This file is part of CPU
     (C) 2003 Blake Matheny (and other contributing authors)

     CPU is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 2, or (at your
     option) any later version.

     CPU is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CPU; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * ldap plugin header file
 * @author Blake Matheny
 * @file ldap.h
 **/

#ifndef LDAP_H
#define LDAP_H

#ifdef  __cplusplus
extern "C" {
#endif

#include "conf.h"
#include "util/parser.h"
#include "util/bitvector.h"
#include "util/hash.h"
#include <stdlib.h>
#include <unistd.h>
#ifdef HAVE_LBER_H
#include <lber.h>
#endif
#include <ldap.h>
#include <sys/time.h>

#define __VERSION "0.0.4"

#ifndef LDAP_OPT_SUCCESS
#define LDAP_OPT_SUCCESS 0
#endif

extern int verbose;
extern int operation;

/* ldap operation type */
typedef enum {
  USERADD = 0,
  USERMOD,
  USERDEL,
  GROUPADD,
  GROUPMOD,
  GROUPDEL,
  CAT,
} ldapop_t;

/* This gross structure holds all options that can be specified at the command
 * line. Some of these can be specified in the config file. Options that can
 * not be passed at the command line, and can only be stored in the config
 * file will be grabbed on the fly. (pg == populateGlobals)
 */
typedef struct CPU_ldap {
  char ** user_object_class;
  char ** group_object_class;
  char ** memberUid;
  char *  bind_dn;	      /* required. BIND_DN or -D (pg) */
  char *  first_name;
  char *  hash;
  char *  hostname;	      /* required. LDAP_HOST or -N (pg) */
  char *  uri;
  char *  skel_directory;
  char *  new_username;
  char *  last_name;
  char *  email_address;
  char *  new_groupname;
  char *  shadow_file;
  char *  password_file;
  char *  add_file;
  char *  bind_password;
  char *  user_base;
  char *  group_base;
  char *  dn;
  char *  cn;
  char *  gid;
  char *  exec;		      /* post {un}install exec script */
  bool    make_home_directory;
  int     port;		      /* required. LDAP_PORT or -P (pg) */
  int     usetls;
  int	  version;
  bool    remove_home_directory;
  bool    assume_yes;
  bool    lock;
  bool    unlock;
  struct cpass * passent;
  struct timeval timeout;
  Parser * parse;
} CPU_ldap;

CPU_ldap * globalLdap;

int parseCommand(int argc, char *argv[]);
void printHelp(int op);
void printUserHelp(int op);
void printGroupHelp(int op);
void printVersion(void);
void CPU_ldapPerror(LDAP *ld, CPU_ldap *opts, char * errmsg);
const char * ldapGetHashPrefix(hash_t hasht);
int initGlobals(void);
int populateGlobals(void);
int ldapOperation(ldapop_t optype);
int ldapUserAdd(LDAP *ld);
int ldapUserMod(LDAP *ld);
int ldapUserDel(LDAP *ld);
int ldapGroupAdd(LDAP *ld);
int ldapGroupMod(LDAP *ld);
int ldapGroupDel(LDAP *ld);
int ldapCat(LDAP *ld);
uid_t getNextUid(LDAP * ld);
gid_t getNextGid(LDAP * ld, ldapop_t op);
int ldapUserCheck(int mod_op, LDAP * ld);
int ldapGroupCheck(int mod_op);
LDAPMod ** ldapBuildListStr(int mod_op, char * mod_type, char * value,
		      LDAPMod ** mods);
LDAPMod ** ldapBuildList(int mod_op, char * mod_type, char ** value,
		      LDAPMod ** mods);
LDAPMod ** ldapBuildListInt(int mod_op, char * mod_type, int value,
		      LDAPMod ** mods);
char * buildDn(ldapop_t op, char *name);
char * ldapGetCn(void);
#ifdef  __cplusplus
}
#endif

#endif
/* end of ldap.h */
