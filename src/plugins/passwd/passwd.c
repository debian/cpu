/*
     This file is part of CPU
     (C) 2003 Blake Matheny (and other contributing authors)

     CPU is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 2, or (at your
     option) any later version.

     CPU is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CPU; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * main method for password routine
 * @author Blake Matheny
 * @file passwd.c
 **/
#include <stdio.h>
#include "plugins/passwd/passwd.h"

int
CPU_init(int argc, char *argv[])
{
  int ret;
  Passwd * pt;


#ifndef DEVELOPER
  fprintf(stderr, "Do not use me. I don't work!\n");
  return 0;
#endif

  if ( (pt = initPass()) == NULL )
    return -1;

  ret = parseCommand(argc, argv, pt);

  if ( ret < 0 )
    return -1;
  if ( ret == 1 )
    return 0;

  if ( populatePass(pt) < 0 )
    return -1;

  if ( passwdOperation(pt) < 0 )
    return -1;

  return 0;
}

int
passwdOperation(Passwd * pt)
{
  /* need to call userAdd, etc. */
  return 0;
}
