/*
     This file is part of CPU
     (C) 2003 Blake Matheny (and other contributing authors)

     CPU is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 2, or (at your
     option) any later version.

     CPU is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CPU; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * take care of all command line options
 * @author Blake Matheny
 * @file commandline.c
 **/
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "plugins/passwd/passwd.h"

int
parseCommand (int argc, char *argv[], Passwd * pt)
{
  int c = 0;
  int options_index = 0;
  extern int optind, opterr;
  struct option long_options[] = {
    DEFAULT_OPTIONS,
    {"defaulthome", 1, 0, 'b'},
    {"comment", 1, 0, 'c'},
    {"homedir", 1, 0, 'd'},
    {"default", 0, 0, 'D'},
    {"expiredate", 1, 0, 'e'},
    {"inactivetime", 1, 0, 'f'},
    {"initialgroup", 1, 0, 'g'},
    {"group", 1, 0, 'G'},
    {"skeldir", 1, 0, 'k'},
    {"loginname", 1, 0, 'l'},
    {"lock", 0, 0, 'L'},
    {"makehome", 0, 0, 'm'},
    {"groupname", 1, 0, 'n'},
    {"duplicate", 0, 0, 'o'},
    {"password", 1, 0, 'p'},
    {"remove", 0, 0, 'r'},
    {"shell", 1, 0, 's'},
    {"uid", 1, 0, 'u'},
    {"unlock", 1, 0, 'U'},
    {0, 0, 0, 0}
  };
  opterr = 1;
  optind = 0;

  while ((c = cgetopt_long (argc, argv,
		"b:c:C:d:De:f:g:G:hk:l:LmM:n:op:rs:u:UV",
		long_options, &options_index)) != -1)
    {
      switch (c)
	{
	  case 'b': /* default_home, with -D */
	  case 'c': /* comment */
	  case 'C': /* default option */
	    break;
	  case 'd': /* home_dir */
	  case 'D': /* Change Defaults */
	  case 'e': /* expire date */
	  case 'f': /* inactive time */
	  case 'g': /* initial group */
	  case 'G': /* other groups */
	  case 'h': /* help */
	    printHelp(-1);
	    return 1;
	    break;
	  case 'k': /* skeleton dir */
	  case 'l': /* new username */
	  case 'L': /* lock, used for usermod */
	  case 'm': /* make home dir */
	  case 'M': /* default option */
	    break;
	  case 'n': /* new group name */
	  case 'o': /* allow create user with duplicate (non-unique) UID */
	  case 'p': /* password */
	  case 'r': /* remove dir */
	  case 's': /* shell */
	  case 'u': /* uid */
	  case 'U': /* unlock, used for usermod */
	  case 'V': /* version */
	    printVersion();
	    return 0;
	    break;
	  default:
	    break;
	}
    }

  if (optind < argc)
    {
      char *mopts = NULL;
      if (argv[optind])
	{
	  mopts = ctolower(argv[optind]);
	  if (strncmp (mopts, "useradd", 7) == 0)
	    pt->op = USERADD;
	  else if (strncmp (mopts, "userdel", 7) == 0)
	    pt->op = USERDEL;
	  else if (strncmp (mopts, "usermod", 7) == 0)
	    pt->op = USERMOD;
	  else if (strncmp (mopts, "groupadd", 8) == 0)
	    pt->op = GROUPADD;
	  else if (strncmp (mopts, "groupdel", 8) == 0)
	    pt->op = GROUPDEL;
	  else if (strncmp (mopts, "groupmod", 8) == 0)
	    pt->op = GROUPMOD;
	  else
	    {
	      CTEST();
	      printHelp (-1);
	      return -1;
	    }
	}
      else
	{
	  CTEST();
	  printHelp (-1);
	  return -1;
	}
    }
  if (!argv[optind + 1])
    {
      CTEST();
      printHelp (pt->op);
      return -1;
    }
      /* it seems there is some corruption of argv, so check for an iscii
         string */
    int k = 0;
    for ( k = 0; k < strlen(argv[optind+1]); k++ )
      {
	if ( !isalpha(argv[optind+1][k]) )
	  {
	    CTEST();
	    printHelp(pt->op);
	    return -1;
	  }
      }
    if ( (pt->p->pw_name = Strdup (argv[optind + 1], "passwd: parseCommand"))
      == NULL)
	return -1;
  return 0;
}

void
printHelp(passop_t op)
{

fprintf (stderr,
"usage: cpu user{add,del,mod} [options] login\n"
"usage: cpu group{add,del,mod} [options] group\n"
);
  switch(op)
    {
      case USERADD:
      case USERMOD:
      case USERDEL:
	printUserHelp(op);
	break;
      case GROUPADD:
      case GROUPMOD:
      case GROUPDEL:
	printGroupHelp(op);
	break;
      default:
fprintf(stderr,
"LDAP Specific Options\n\n"

"\t-A cn, --cn=cn                   : Comman Name Prefix\n"
"\t-B group_base, --groupbase=base  : Base DN for groups\n"
"\t-D bind_dn, --binddn=bind_dn     : Bind DN\n"
"\t-H hash, --hash=hash             : Password Hash Type\n"
"\t-N hostname, --hostname=hostname : LDAP Server\n"
"\t-P port, --port=port             : Port\n"
"\t--shadow=[file]                  : Shadow Pass File\n"
"\t-t timeout, --timeout=timeout    : Timeout\n"
"\t-U base, --userbase=base         : Base DN for users\n"
"\t--bindpass=[pass]                : Bind Password\n\n"

"Attributes that can be added and modified\n\n"
"\t-f name, --firstname=name        : First Name\n"
"\t-L name, --lastname=name         : Last Name\n"
"\t-e address, --email=address      : Email Address\n\n"

"Options that impact all options\n\n"
"\t-y, --yes                        : Yes to All\n"
"\t-h, --help                       : This help\n"
"\t-v, --verbose                    : Verbose\n"
"\t-V, --version                    : Version\n\n"

);
	break;
    }
  return;
}

void
printUserHelp(passop_t op)
{
  switch(op)
    {
      case USERADD:
/* don't support -r -e -f -o */
fprintf(stderr,
"usage: cpu useradd [options] login\n"
"\t-c comment --gecos=COMMENT       : Gecos Information\n"
"\t-d home_dir --directory=home_dir : Users home directory\n"
"\t-g initial_group --gid=integer   : The group id of the user's\n"
"\t                                   initial login group\n"
"\t-G group,[...] --sgroup=group,[] : A list of supplementary groups\n"
"\t-k skeleton_dir --skel=dir       : The skeleton directory\n\n"
"\t-m --makehome                    : The user's home directory will be\n"
"\t                                   created if it does not exist\n"
"\t-p passwd --password=password    : The unencrypted password\n"
"\t-s shell --shell=shell           : The name of the user's login shell\n"
"\t-u uid --uid=uid                 : The numerical value of the user's\n"
"\t                                   ID.\n\n"
);
	break;
      case USERMOD:
/* don't support -e -f -L -U -o */
fprintf(stderr,
"usage: cpu usermod [options] login\n"
"\t-c comment --gecos=COMMENT       : Gecos Information\n"
"\t-d home_dir --directory=home_dir : Users home directory\n"
"\t-g initial_group --gid=integer   : The group id of the user's initial\n"
"\t                                   group\n"
"\t-G group,[...] --sgroup=group,[] : A list of supplementary groups\n"
"\t-l login_name, --newusername=NAME: New user name\n\n"
"\t-m --makehome                    : The user's old directory will be\n"
"\t                                   copied to the new location\n"
"\t-p passwd --password=password    : The unencrypted password\n"
"\t-s shell --shell=shell           : The name of the user's login shell\n"
"\t-u uid --uid=uid                 : The numerical value of the user's\n"
"\t                                   ID.\n\n"
);
	break;
      case USERDEL:
fprintf(stderr,
"usage: cpu userdel [options] login\n"
"\t-r, --removehome                 : Remove user's home directory\n\n"
);
	break;
      default:
	break;
    }
  return;
}

void
printGroupHelp(passop_t op)
{
  switch(op)
    {
      case GROUPADD:
/* don't support -o or -r */
fprintf(stderr,
"usage: cpu groupadd [options] group\n"
"\t-g gid --gid=gid                 : The numeric value of the group id\n\n"
);
	break;
      case GROUPMOD:
/* don't support -o */
fprintf(stderr,
"usage: cpu groupmod [options] group\n"
"\t-g gid --gid=gid                 : The numeric value of the group id\n\n"
"\t-n group_name --newgroupname=NAME: The name that group will change to\n\n"
);
	break;
      case GROUPDEL:
fprintf(stderr,
"usage: cpu groupdel group\n\n"
);
	break;
      default:
	break;
    }
  return;
}

void
printVersion (void)
{
  fprintf (stderr, "libcpu_passwd (CPU) %s\n"
	   "Written by Blake Matheny\n"
	   "Copyright 2001, 2002, 2003\n\n"
	   "This is free software; see the source for copying "
	   "conditions. There is NO\nwarranty; not even for "
	   "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n",
	   __VERSION);
  return;
}
