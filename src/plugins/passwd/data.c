/*
     This file is part of CPU
     (C) 2003 Blake Matheny (and other contributing authors)

     CPU is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 2, or (at your
     option) any later version.

     CPU is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CPU; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * deal with initializing values and populating structures
 * @author Blake Matheny
 * @file data.c
 **/
#include <stdio.h>
#include <string.h>
#include "plugins/passwd/passwd.h"

int populateUserAdd(Passwd * pt);
int populateUserMod(Passwd * pt);
int populateUserDel(Passwd * pt);

int
populatePass(Passwd * pt)
{
  if ( pt == NULL )
    return -1;

  /* fix me */
  if ( cfg_get_str("PASSWD", "PASSWORD") == NULL )
    return -1;

  switch(pt->op)
    {
      case USERADD:
	return populateUserAdd(pt);
	break;
      case USERMOD:
	return populateUserMod(pt);
	break;
      case USERDEL:
	return populateUserDel(pt);
	break;
      case GROUPADD:
	{
	}
	break;
      case GROUPMOD:
	{
	}
	break;
      case GROUPDEL:
	{
	}
	break;
      default:
	break;
    }
  return 0;
}

Passwd *
initPass(void)
{
  Passwd * pt = NULL;

  pt = (Passwd*)malloc(sizeof(Passwd));
  if ( pt == NULL )
    return pt;
  bzero(pt, sizeof(Passwd));

  pt->lock = false;
  pt->unlock = false;
  pt->md = false;
  pt->rmd = false;

  pt->p = (struct cpass*)malloc(sizeof(struct cpass));
  if ( pt->p == NULL )
    return NULL;
  bzero(pt->p, sizeof(struct cpass));

  pt->p->pw_uid = -1;
  pt->p->pw_gid = -1;
  return pt;
}

int
populateUserDel(Passwd * pt)
{
  if ( cgetpwent(cfg_get_str("PASSWD", "PASSWORD"), pt->p->pw_name, PASSWORD)
       == NULL )
    return -1;
  if ( cgetpwent(cfg_get_str("PASSWD", "SHADOW"), pt->p->pw_name, SHADOW)
       == NULL)
    return -1;
  return 0;
}

int
populateUserMod(Passwd * pt)
{
  if ( pt->lock == false && pt->unlock == false && pt->md == false &&
       pt->groups == NULL && pt->newname == NULL && pt->p->pw_name == NULL &&
       pt->p->pw_passwd == NULL && pt->p->pw_uid == -1 && pt->p->pw_gid == -1
       && pt->p->pw_gecos == NULL && pt->p->pw_dir == NULL &&
       pt->p->pw_shell == NULL && pt->p->sp_inact == -10 &&
       pt->p->sp_expire == -1 )
    return -1;
  return 0;
}

int
populateUserAdd(Passwd * pt)
{
  if ( pt->p->pw_passwd == NULL )
    pt->p->pw_passwd = strdup("*");
  if ( pt->p->pw_uid == -1 )
    pt->p->pw_uid = getNextUid();
  if ( pt->p->pw_uid == -1 )
    return -1;
  if ( pt->p->pw_gid == -1 )
    pt->p->pw_gid = cfg_get_int("PASSWD", "GROUP");
  if ( pt->p->pw_gid < 0 )
    return -1;
  if ( pt->p->pw_gecos == NULL )
    pt->p->pw_gecos = cfg_get_str("PASSWD", "COMMENT");
  if ( pt->p->pw_gecos == NULL )
    pt->p->pw_gecos = "";
  if ( pt->p->pw_dir == NULL )
    pt->p->pw_dir = getHomeDir(cfg_get_str("PASSWD", "HOME"),
			       pt->p->pw_name);
  if ( pt->p->pw_dir == NULL )
    return -1;
  if ( pt->p->pw_shell == NULL )
    pt->p->pw_shell = cfg_get_str("PASSWD", "SHELL");
  if ( pt->p->pw_shell == NULL )
    return -1;
  if ( pt->md == true )
    {
      if ( pt->skeldir == NULL )
	pt->skeldir = cfg_get_str("PASSWD", "SKEL");
    }
  return 0;
}

