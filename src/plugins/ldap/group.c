/*
     This file is part of CPU
     (C) 2003 Blake Matheny (and other contributing authors)

     CPU is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 2, or (at your
     option) any later version.

     CPU is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CPU; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * group{add,mod,del} functions and helpers
 * @author Blake Matheny
 * @file group.c
 **/
#include <stdio.h>
#include "plugins/ldap/ldap.h"

LDAPMod **groupMod = NULL;

int
ldapGroupAdd (LDAP * ld)
{
  if (ldapGroupCheck (LDAP_MOD_ADD) < 0)
    {
      fprintf (stderr, "ldap: ldapGroupAdd: error in ldapGroupCheck\n");
      return -1;
    }
  if (ldap_add_s (ld, globalLdap->dn, groupMod) != LDAP_SUCCESS)
    {
      CPU_ldapPerror (ld, globalLdap, "ldapGroupAdd: ldap_add_s");
      return -1;
    }
  fprintf (stdout, "Group %s successfully added!\n",
	   globalLdap->passent->pw_name);
  return 0;
}

int
ldapGroupMod (LDAP * ld)
{
  char *newdn = NULL;
  if (ldapGroupCheck (LDAP_MOD_REPLACE) < 0)
    {
      fprintf (stderr, "ldap: ldapGroupMod: error in ldapGroupCheck\n");
      return -1;
    }
  if (groupMod == NULL && globalLdap->new_groupname == NULL)
    {
      fprintf (stderr, "ldap: ldapGroupMod: No Modification requested\n");
      return 0;
    }

  if (groupMod != NULL)
    {
      if (ldap_modify_s (ld, globalLdap->dn, groupMod) != LDAP_SUCCESS)
	{
	  CPU_ldapPerror (ld, globalLdap, "ldapGroupMod: ldap_modify_s");
	  return -1;
	}
    }

  if (globalLdap->new_groupname)
    {
      newdn = buildDn (GROUPMOD, globalLdap->new_groupname);
      if (newdn == NULL)
	return -1;
      if (ldap_modrdn2_s (ld, globalLdap->dn, newdn, 1) != LDAP_SUCCESS)
	{
	  CPU_ldapPerror (ld, globalLdap, "ldapGroupMod: ldap_modrdn_s");
	  return -1;
	}
      free (newdn);
      globalLdap->passent->pw_name = globalLdap->new_groupname;
      newdn = buildDn (GROUPADD, globalLdap->new_groupname);
      globalLdap->dn = newdn;
    }

  fprintf (stdout, "Group %s successfully modified!\n",
	   globalLdap->passent->pw_name);
  return 0;
}

int
ldapGroupDel (LDAP * ld)
{
  if (ldap_delete_s (ld, globalLdap->dn) != LDAP_SUCCESS)
    {
      CPU_ldapPerror (ld, globalLdap, "ldapGroupDel: ldap_delete_s");
      return -1;
    }
  fprintf (stdout, "Group %s successfully deleted!\n",
	   globalLdap->passent->pw_name);
  return 0;
}

int
ldapGroupCheck (int mod_op)
{
  int op = 0;
  if (mod_op == LDAP_MOD_ADD)
    op = LDAP_MOD_ADD;
  else if (mod_op == LDAP_MOD_REPLACE)
    op = LDAP_MOD_REPLACE;
  else
    return -1;

  /* the following is from rfc2307
     nisSchema.2.0 NAME 'posixGroup' SUP top STRUCTURAL
     DESC 'Abstraction of a group of accounts'
     MUST ( cn $ gidNumber )
     MAY ( userPassword $ memberUid $ description )
   */
  if (op == LDAP_MOD_ADD)
    {
      groupMod = ldapBuildList (op, "objectClass",
				globalLdap->group_object_class, groupMod);
      groupMod =
	ldapBuildListStr (LDAP_MOD_ADD, "cn", ldapGetCn (), groupMod);
    }

  if (globalLdap->passent->pw_passwd)
    groupMod = ldapBuildListStr (op, "userPassword",
				 globalLdap->passent->pw_passwd, groupMod);

  if ((int) globalLdap->passent->pw_gid > -1)
    groupMod =
      ldapBuildListInt (op, "gidNumber", (int) globalLdap->passent->pw_gid,
			groupMod);
  if (globalLdap->parse != NULL)
    {
      Parser *pos;
      pos = globalLdap->parse;
      while (pos != NULL)
	{
	  groupMod = ldapBuildListStr (op, pos->attr, pos->attrval, groupMod);
	  pos = pos->next;
	}
    }

  return 0;
}
