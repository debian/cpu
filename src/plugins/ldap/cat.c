/*
     This file is part of CPU
     (C) 2003 Blake Matheny (and other contributing authors)

     CPU is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 2, or (at your
     option) any later version.

     CPU is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CPU; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * cat functions and helpers
 * @author Blake Matheny
 * @file cat.c
 **/
#include <stdio.h>
#include <string.h>
#include "plugins/ldap/ldap.h"

int
ldapCat (LDAP * ld)
{
  LDAPMessage *res[2];
  LDAPMessage *pos;
  BerElement *ber;
  int i = 0;
  int ldapres = 0;
  int j = 0;
  int lsize = 0;
  struct cpass passentry;
  struct timeval timeout;
  char **vals;
  char **memberUid;
  char *a;
  char *filter;
  char *attrs[7] = {
    "uid",
    "uidNumber",
    "gidNumber",
    "gecos",
    "homeDirectory",
    "loginShell",
    NULL
  };
  char *gattr[4] = {
    "cn",
    "gidNumber",
    "memberUid",
    NULL,
  };
  timeout = globalLdap->timeout;

  filter = cfg_get_str ("LDAP", "USER_FILTER");
  if (filter == NULL)
    filter = strdup ("(objectClass=posixAccount)");

  res[1] = NULL;

  if (ldap_search_st (ld, globalLdap->user_base, LDAP_SCOPE_SUBTREE, filter,
	attrs, 0, &timeout, res) != LDAP_SUCCESS)
  {
    CPU_ldapPerror (ld, globalLdap, "ldapCat: ldap_search_st");
    return -1;
  }

  ldapres = ldap_count_entries (ld, res[0]);
  pos = ldap_first_entry (ld, res[0]);
  fprintf (stdout, "User Accounts\n");
  for (i = 0; i < ldapres; i++)
  {
    passentry.pw_uid = passentry.pw_gid = 0;
    passentry.pw_name = passentry.pw_gecos = passentry.pw_dir = NULL;
    passentry.pw_shell = NULL;
    for (a = ldap_first_attribute (ld, pos, &ber); a != NULL;
	a = ldap_next_attribute (ld, pos, ber))
    {
      if ((vals = ldap_get_values (ld, pos, a)) != NULL)
      {
	for (j = 0; vals[j] != NULL; j++)
	{
	  if (strncmp (a, "uidNumber", 9) == 0)
	    passentry.pw_uid = atoi (vals[j]);
	  else if (strncmp (a, "uid", 3) == 0)
	    passentry.pw_name = strdup (vals[j]);
	  else if (strncmp (a, "gidNumber", 9) == 0)
	    passentry.pw_gid = atoi (vals[j]);
	  else if (strncmp (a, "gecos", 5) == 0)
	    passentry.pw_gecos = strdup (vals[j]);
	  else if (strncmp (a, "homeDirectory", 13) == 0)
	    passentry.pw_dir = strdup (vals[j]);
	  else if (strncmp (a, "loginShell", 10) == 0)
	    passentry.pw_shell = strdup (vals[j]);
	}
      }
    }
    if (passentry.pw_gecos == NULL)
      passentry.pw_gecos = "";
    if (passentry.pw_shell == NULL)
      passentry.pw_shell = "";
    if (passentry.pw_name != NULL)
      fprintf (stdout, "%s:x:%d:%d:%s:%s:%s\n", passentry.pw_name,
	  (int)passentry.pw_uid, (int)passentry.pw_gid,
	  passentry.pw_gecos, passentry.pw_dir, passentry.pw_shell);
    Free (passentry.pw_name);
    if (passentry.pw_gecos != "")
      Free (passentry.pw_gecos);
    Free (passentry.pw_dir);
    if (passentry.pw_shell != "")
      Free (passentry.pw_shell);
    pos = ldap_next_entry (ld, pos);
  }

  filter = NULL;
  filter = cfg_get_str ("LDAP", "GROUP_FILTER");
  if (filter == NULL)
    filter = strdup ("(objectClass=posixGroup)");

  res[1] = NULL;

  if (ldap_search_st (ld, globalLdap->group_base, LDAP_SCOPE_SUBTREE, filter,
	gattr, 0, &timeout, res) != LDAP_SUCCESS)
  {
    CPU_ldapPerror (ld, globalLdap, "ldapCat: ldap_search_st");
    return -1;
  }

  ldapres = 0;
  ldapres = ldap_count_entries (ld, res[0]);
  pos = NULL;
  pos = ldap_first_entry (ld, res[0]);
  fprintf (stdout, "\nGroup Entries\n");
  for (i = 0; i < ldapres; i++)
  {
    passentry.pw_name = NULL;
    passentry.pw_gid = 0;
    lsize = 0;
    memberUid = NULL;
    for (a = ldap_first_attribute (ld, pos, &ber); a != NULL;
	a = ldap_next_attribute (ld, pos, ber))
    {
      if ((vals = ldap_get_values (ld, pos, a)) != NULL)
      {
	for (j = 0; vals[j] != NULL; j++)
	{
	  if (strncmp (a, "gidNumber", 9) == 0)
	    passentry.pw_gid = atoi (vals[j]);
	  else if (strncmp (a, "cn", 2) == 0)
	    passentry.pw_name = strdup (vals[j]);
	  else if (strncmp (a, "memberUid", 9) == 0)
	  {
	    memberUid = (char **) realloc (memberUid,
		sizeof (char *) *
		(lsize + 2));
	    if (memberUid == NULL)
	      return -1;
	    memberUid[lsize] =
	      (char *) malloc (sizeof (char) * strlen (vals[j]));
	    if (memberUid[lsize] == NULL)
	      return -1;
	    bzero (memberUid[lsize],
		sizeof (char) * strlen (vals[j]));
	    memberUid[lsize] = strdup (vals[j]);
	    memberUid[lsize + 1] = NULL;
	    lsize++;
	  }
	}
      }
    }
    if (passentry.pw_gecos == NULL)
      passentry.pw_gecos = "";
    if (passentry.pw_shell == NULL)
      passentry.pw_shell = "";
    if (passentry.pw_name != NULL)
    {
      fprintf (stdout, "%s:x:%d:", passentry.pw_name, (int)passentry.pw_gid);
      if (memberUid != NULL)
	for (j = 0; memberUid[j] != NULL; j++)
	{
	  fprintf (stdout, "%s", memberUid[j]);
	  if (memberUid[j + 1] == NULL)
	    fprintf (stdout, "\n");
	  else
	    fprintf (stdout, ", ");
	}
      else
	printf ("\n");
    }
    Free (passentry.pw_name);
    Free (memberUid);
    pos = ldap_next_entry (ld, pos);
  }

  return 0;
}
