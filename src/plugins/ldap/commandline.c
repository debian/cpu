/*
     This file is part of CPU
     (C) 2003 Blake Matheny (and other contributing authors)

     CPU is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 2, or (at your
     option) any later version.

     CPU is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CPU; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * take care of all command line options
 * @author Blake Matheny
 * @file commandline.c
 **/
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "plugins/ldap/ldap.h"

int
parseCommand (int argc, char *argv[])
{
  int c = 0;
  int options_index = 0;
  struct cpass *passent;
  bool vp = false;
  extern int optind, opterr;
  struct option long_options[] = {
    DEFAULT_OPTIONS,
    {"2", 1, 0, '2'},
    {"addfile", 1, 0, 'a'},
    {"cn", 1, 0, 'A'},
    {"userbase", 1, 0, 'b'},
    {"groupbase", 1, 0, 'B'},
    {"gecos", 1, 0, 'c'},
    {"directory", 1, 0, 'd'},
    {"binddn", 1, 0, 'D'},
    {"email", 1, 0, 'e'},
    {"lastname", 1, 0, 'E'},
    {"firstname", 1, 0, 'f'},
    {"passfile", 2, 0, 'F'},
    {"gid", 1, 0, 'g'},
    {"sgroups", 1, 0, 'G'},
    {"hash", 1, 0, 'H'},
    {"skel", 2, 0, 'k'},
    {"newusername", 1, 0, 'l'},
    {"lock", 0, 0, 'L'},
    {"makehome", 0, 0, 'm'},
    {"newgroupname", 1, 0, 'n'},
    {"hostname", 1, 0, 'N'},
    {"nonposix", 0, 0, 'o'},
    {"password", 2, 0, 'p'},
    {"port", 1, 0, 'P'},
    {"removehome", 0, 0, 'r'},
    {"random", 1, 0, 'R'},
    {"shell", 1, 0, 's'},
    {"shadfile", 2, 0, 'S'},
    {"timeout", 1, 0, 't'},
    {"uid", 1, 0, 'u'},
    {"unlock", 0, 0, 'U'},
    {"bindpass", 2, 0, 'w'},
    {"tls", 0, 0, 'x'},
    {"exec", 1, 0, 'X'},
    {"yes", 0, 0, 'y'},
    {"uri", 1, 0, 'Z'},
    {0, 0, 0, 0}
  };
  opterr = 1;
  optind = 0;

  passent = (struct cpass *) malloc (sizeof (struct cpass));
  if (passent == NULL)
    return -1;
  bzero (passent, sizeof (struct cpass));
  (int) passent->sp_lstchg = passent->sp_min = passent->sp_max = -10;
  passent->sp_warn = passent->sp_inact = passent->sp_expire = -10;
  passent->sp_flag = -10;
  passent->pw_gid = -10;
  passent->pw_uid = -10;

  while ((c = cgetopt_long (argc, argv,
	  "2a:A:b:B:c:C:d:D:e:E:f:F::g:G:h:H:k::l:LmM:n:N:op::P:rR:s:S::t:u:UvVw::xX:yZ:",
	  long_options, &options_index)) != -1)
  {
      switch (c)
      {
	  case '2':
	      globalLdap->version = 2;
	      break;
	  case 'a':
	      globalLdap->add_file = strdup (optarg);
	      break;
	  case 'A':
	      globalLdap->cn = strdup (optarg);
	      break;
	  case 'b':
	      globalLdap->user_base =
		  Strdup (optarg, "DEBUG: ldap: parseCommand\n");
	      if (globalLdap->user_base == NULL)
		  return -1;
	      break;
	  case 'B':
	      globalLdap->group_base = strdup (optarg);
	      break;
	  case 'c':
	      if (optarg != NULL)
	      {
		  passent->pw_gecos =
		      Strdup (optarg, "DEBUG: ldap: parseCommand\n");
		  if (passent->pw_gecos == NULL)
		      return -1;
	      }
	      break;
	  case 'd':
	      passent->pw_dir = Strdup (optarg, "DEBUG: ldap: parseCommand\n");
	      if (passent->pw_dir == NULL)
		  return -1;
	      break;
	  case 'D':
	      globalLdap->bind_dn =
		  Strdup (optarg, "DEBUG: ldap: parseCommand\n");
	      if (globalLdap->bind_dn == NULL)
		  return -1;
	      break;
	  case 'e':
	      globalLdap->email_address =
		  Strdup (optarg, "DEBUG: ldap: parseCommand\n");
	      if (globalLdap->email_address == NULL)
		  return -1;
	      break;
	  case 'E':
	      globalLdap->last_name =
		  Strdup (optarg, "DEBUG: ldap: parseCommand\n");
	      if (globalLdap->last_name == NULL)
		  return -1;
	      break;
	  case 'f':
	      globalLdap->first_name =
		  Strdup (optarg, "DEBUG: ldap: parseCommand\n");
	      if (globalLdap->first_name == NULL)
		  return -1;
	      break;
	  case 'F':
	      if (optarg)
	      {
		  globalLdap->password_file = strdup (optarg);
	      }
	      else
	      {
		  globalLdap->password_file =
		      cfg_get_str ("LDAP", "PASSWORD_FILE");
	      }
	      break;
	  case 'g':
	      if (isdigit ((int)optarg[0]))
		  passent->pw_gid = atoi (optarg);
	      else
		  globalLdap->gid = strdup (optarg);
	      break;
	  case 'G':
	      {
		  char *gtemp = NULL;
		  int num_tokens = 0;
		  if (isalnum ((int)optarg[0]))
		      gtemp = strdup (optarg);
		  if (gtemp == NULL)
		      return -1;
		  while (gtemp != NULL && *gtemp)
		  {
		      globalLdap->memberUid =
			  (char **) realloc (globalLdap->memberUid,
					     sizeof(char*)*((num_tokens+1)*4));
		      globalLdap->memberUid[num_tokens] = getToken(&gtemp,",");
		      num_tokens++;
		  }
		  globalLdap->memberUid[num_tokens] = NULL;
		  break;
	      }
	  case 'H':
	      globalLdap->hash = Strdup (optarg, "DEBUG: ldap: parseCommand\n");
	      if (globalLdap->hash == NULL)
		  return -1;
	      break;
	  case 'k':
	      if (optarg)
	      {
		  globalLdap->skel_directory =
		      Strdup (optarg, "DEBUG: ldap: parseCommand\n");
		  if (globalLdap->skel_directory == NULL)
		      return -1;
	      }
	      else
		  globalLdap->skel_directory = cfg_get_str ("LDAP", "SKEL_DIR");
	      break;
	  case 'l':
	      globalLdap->new_username =
		  Strdup (optarg, "DEBUG: ldap: parseCommand\n");
	      if (globalLdap->new_username == NULL)
		  return -1;
	      break;
	  case 'L':
	      globalLdap->lock = true;
	      break;
	  case 'm':
	      globalLdap->make_home_directory = true;
	      break;
	  case 'n':
	      globalLdap->new_groupname =
		  Strdup (optarg, "DEBUG: ldap: parseCommand\n");
	      if (globalLdap->new_groupname == NULL)
		  return -1;
	      break;
	  case 'N':
	      globalLdap->hostname =
		  Strdup (optarg, "DEBUG: ldap: parseCommand\n");
	      if (globalLdap->hostname == NULL)
		  return -1;
	      break;
	  case 'o':
	      vp = true;
	      break;
	  case 'p':
	      if (optarg)
	      {
		  passent->pw_passwd =
		      Strdup (optarg, "DEBUG: ldap: parseCommand\n");
		  if (passent->pw_passwd == NULL)
		      return -1;
	      }
	      else
	      {
		  passent->pw_passwd =
		      CPU_getpass ("Please enter desired user password: ");
		  if (passent->pw_passwd == NULL)
		      return -1;
	      }
	      break;
	  case 'P':
	      globalLdap->port = atoi (optarg);
	      break;
	  case 'r':
	      globalLdap->remove_home_directory = true;
	      break;
	  case 'R':
	      passent->pw_passwd = genPass (atoi (optarg));
	      fprintf (stdout, "Generated password: %s\n", passent->pw_passwd);
	      break;
	  case 's':
	      passent->pw_shell=Strdup (optarg, "DEBUG: ldap: parseCommand\n");
	      if (passent->pw_shell == NULL)
		  return -1;
	      break;
	  case 'S':		/* shadow */
	      if (optarg)
	      {
		  globalLdap->shadow_file =
		      Strdup (optarg, "DEBUG: ldap: parseCommand\n");
		  if (globalLdap->shadow_file == NULL)
		      return -1;
	      }
	      else
	      {
		  globalLdap->shadow_file = cfg_get_str ("LDAP", "SHADOW_FILE");
		  if (globalLdap->shadow_file == NULL)
		      return -1;
	      }
	      break;
	  case 't':
	      globalLdap->timeout.tv_sec = atoi (optarg);
	      break;
	  case 'u':
	      passent->pw_uid = atoi (optarg);
	      break;
	  case 'U':
	      globalLdap->unlock = true;
	      break;
	  case 'v':
	      verbose = 1;
	      break;
	  case 'w':		/* bind pass */
	      if (optarg)
	      {
		  globalLdap->bind_password =
		      Strdup (optarg, "DEBUG: ldap: parseCommand\n");
		  if (globalLdap->bind_password == NULL)
		      return -1;
	      }
	      else
	      {
		  globalLdap->bind_password =
		      CPU_getpass ("Please enter the LDAP bind password: ");
		  if (globalLdap->bind_password == NULL)
		      return -1;
	      }
	      break;
	  case 'x':
	      globalLdap->usetls = 1;
	      break;
	  case 'X':
	      globalLdap->exec = Strdup (optarg, "DEBUG: ldap: parseCommand\n");
	      break;
	  case 'y':
	      globalLdap->assume_yes = true;
	      break;
	  case 'Z':
	      globalLdap->uri = Strdup (optarg, "DEBUG: ldap: parseCommand\n");
	      if (globalLdap->uri == NULL)
		  return -1;
	      break;
	  case 'h':
	      printHelp (-1);
	      return 1;
	      break;
	  case 'V':
	      printVersion ();
	      return 1;
	      break;
	  case 'C':
	  case 'M':
	  default:
	      break;
      }
  }

  CTEST();
  if (optind < argc)
  {
    char *mopts = NULL;
    if (argv[optind])
    {
      mopts = ctolower (argv[optind]);
      if (strncmp (mopts, "useradd", 7) == 0)
	operation = USERADD;
      else if (strncmp (mopts, "userdel", 7) == 0)
	operation = USERDEL;
      else if (strncmp (mopts, "usermod", 7) == 0)
	operation = USERMOD;
      else if (strncmp (mopts, "groupadd", 8) == 0)
	operation = GROUPADD;
      else if (strncmp (mopts, "groupdel", 8) == 0)
	operation = GROUPDEL;
      else if (strncmp (mopts, "groupmod", 8) == 0)
	operation = GROUPMOD;
      else if (strncmp (mopts, "cat", 3) == 0)
	operation = CAT;
      else
      {
	CTEST ();
	printHelp (-1);
	return -1;
      }
    }
    else
    {
      CTEST ();
      printHelp (-1);
      return -1;
    }
  }
  else if (operation != CAT)
  {
    CTEST ();
    printHelp (operation);
    return -1;
  }

  if (operation != CAT)
  {
    int k = 0;
    if (argv[optind + 1] == NULL)
    {
      CTEST ();
      printHelp (operation);
      return -1;
    }
    for (k = 0; k < (int) strlen (argv[optind + 1]); ++k)
    {
      CTEST();
      if (k == 0)
      {
	if (argv[optind + 1][k] == '-')
	{
	  CTEST ();
	  printHelp (operation);
	  return -1;
	}
      }
      CTEST();

      if ( !vp) {
	if (!isalnum ((int)argv[optind + 1][k]) &&
	    argv[optind + 1][k] != '.' &&
	    argv[optind + 1][k] != '-' && argv[optind + 1][k] != '_')
	{
	  CTEST ();
	  printHelp (operation);
	  return -1;
	}
      }
    }
    if ((passent->pw_name =
	  Strdup (argv[optind + 1], "DEBUG: ldap: parseCommand\n")) == NULL)
      return -1;
  }
  globalLdap->passent = passent;
  return 0;
}

int
initGlobals (void)
{
  globalLdap = (CPU_ldap *) malloc (sizeof (CPU_ldap));
  if (globalLdap == NULL)
    return -1;
  bzero (globalLdap, sizeof (CPU_ldap));
  globalLdap->make_home_directory = false;
  globalLdap->remove_home_directory = false;
  globalLdap->assume_yes = false;
  globalLdap->lock = false;
  globalLdap->unlock = false;
  globalLdap->port = -1;
  globalLdap->cn = NULL;
  globalLdap->exec = NULL;
  globalLdap->timeout.tv_sec = -10;
  globalLdap->version = 3;
  return 0;
}

int
populateGlobals (void)
{
  char *object_vals = NULL;
  int num_tokens = 0;
  Parser *p;

  /* start of required fields */
  if (globalLdap->hostname == NULL && globalLdap->uri == NULL)
    {
      if (((globalLdap->hostname = cfg_get_str ("LDAP", "LDAP_HOST")) == NULL)
	  && ((globalLdap->uri = cfg_get_str ("LDAP", "LDAP_URI")) == NULL))
	{
	  fprintf (stderr, "ldap: populateGlobals: LDAP_HOST or LDAP_URI not "
		   "found in configuration file or specified at command line. "
		   "Error.\n");
	  return -1;
	}
    }
  if (globalLdap->uri == NULL)
    {
      if (globalLdap->port == -1)
	{
	  if ((globalLdap->port = cfg_get_int ("LDAP", "LDAP_PORT")) == -10)
	    {
	      fprintf (stderr,
		       "ldap: populateGlobals: LDAP_PORT not found in "
		       "configuration file or specified at command line. "
		       "Error.\n");
	      return -1;
	    }
	}
      if (globalLdap->port < 0 || globalLdap->port > 65535)
	{
	  fprintf (stderr, "ldap: populateGlobals: Invalid LDAP_PORT "
		   "specified. Error.\n");
	  return -1;
	}
    }
  if (globalLdap->bind_dn == NULL)
    {
      if ((globalLdap->bind_dn = cfg_get_str ("LDAP", "BIND_DN")) == NULL)
	{
	  fprintf (stderr, "ldap: populateGlobals: BIND_DN not found in "
		   "config file or specified at command line. " "Error.\n");
	  return -1;
	}
    }
  if (globalLdap->bind_password == NULL)
    {
      globalLdap->bind_password = cfg_get_str ("LDAP", "BIND_PASS");
    }

  if (globalLdap->timeout.tv_sec == -10)
    {
      globalLdap->timeout.tv_sec = (long) cfg_get_int ("LDAP", "TIMEOUT");
      if (globalLdap->timeout.tv_sec == -10)
	globalLdap->timeout.tv_sec = 60;
    }
  globalLdap->timeout.tv_usec = 0;

  if (globalLdap->group_base == NULL)
    {
      if ((globalLdap->group_base = cfg_get_str ("LDAP", "GROUP_BASE"))
	  == NULL)
	{
	  fprintf (stderr, "GROUP_BASE not found in configuration file or"
		   " specified at command line but is required "
		   " for operation. Error.\n");
	  return -1;
	}
    }

  /* end of required fields */

  if (operation == USERADD && globalLdap->password_file != NULL)
    {
      struct cpass *p = NULL;
      p = cgetpwent (globalLdap->password_file, globalLdap->passent->pw_name,
		     PASSWORD);
      if (p == NULL)
	{
	  fprintf (stderr, "Could not find user '%s' in file '%s'\n",
		   globalLdap->passent->pw_name, globalLdap->password_file);
	}
      else
	{
	  globalLdap->passent->pw_name = strdup (p->pw_name);
	  if (globalLdap->passent->pw_gecos == NULL)
	    globalLdap->passent->pw_gecos = strdup (p->pw_gecos);
	  if ((int) globalLdap->passent->pw_uid < 0)
	    globalLdap->passent->pw_uid = p->pw_uid;
	  if ((int) globalLdap->passent->pw_gid < 0)
	    globalLdap->passent->pw_gid = p->pw_gid;
	  if (globalLdap->passent->pw_gecos == NULL)
	    globalLdap->passent->pw_gecos = strdup (p->pw_gecos);
	  if (globalLdap->passent->pw_dir == NULL)
	    globalLdap->passent->pw_dir = strdup (p->pw_dir);
	  if (globalLdap->passent->pw_shell == NULL)
	    globalLdap->passent->pw_shell = strdup (p->pw_shell);
	}
    }
  if (operation == USERADD && globalLdap->shadow_file != NULL)
    {
      struct cpass *p;
      p = cgetpwent (globalLdap->shadow_file, globalLdap->passent->pw_name,
		     SHADOW);
      if (p)
	{
	  if (globalLdap->passent->pw_passwd == NULL)
	    globalLdap->passent->pw_passwd = strdup (p->pw_passwd);
	  globalLdap->passent->sp_lstchg = p->sp_lstchg;
	  globalLdap->passent->sp_min = p->sp_min;
	  globalLdap->passent->sp_max = p->sp_max;
	  globalLdap->passent->sp_warn = p->sp_warn;
	  globalLdap->passent->sp_inact = p->sp_inact;
	  globalLdap->passent->sp_expire = p->sp_expire;
	  globalLdap->passent->sp_flag = p->sp_flag;
	}
      else
	{
	  fprintf (stderr, "Could not find user '%s' in file '%s'\n",
		   globalLdap->passent->pw_name, globalLdap->shadow_file);
	}
    }
  else if ( operation == USERADD ||
            (operation == USERMOD && (globalLdap->lock || globalLdap->unlock)) )
    {
      const char *shadlist[] = { "SHADOWLASTCHANGE", "SHADOWMAX",
                                 "SHADOWWARING", "SHADOWEXPIRE", "SHADOWFLAG",
                                 "SHADOWMIN", "SHADOWINACTIVE", NULL };
      int shadctr;
      char *shadtmp = NULL;
      for ( shadctr = 0; shadlist[shadctr] != NULL; ++shadctr ) {
        shadtmp = getenv(shadlist[shadctr]);
        if ( shadtmp == NULL )
          shadtmp = cfg_get_str("LDAP", shadlist[shadctr]);
        if ( shadtmp != NULL ) {
          switch(shadctr) {
                  case 0:
                          globalLdap->passent->sp_lstchg = atoi(shadtmp);
                          break;
                  case 1:
                          globalLdap->passent->sp_max = atoi(shadtmp);
                          break;
                  case 2:
                          globalLdap->passent->sp_warn = atoi(shadtmp);
                          break;
                  case 3:
                          globalLdap->passent->sp_expire = atoi(shadtmp);
                          break;
                  case 4:
                          globalLdap->passent->sp_flag = atoi(shadtmp);
                          break;
                  case 5:
                          globalLdap->passent->sp_min = atoi(shadtmp);
                          break;
                  case 6:
                          globalLdap->passent->sp_inact = atoi(shadtmp);
                          break;
                  default:
                          break;
          }
        }
      }
    }

  if (operation == USERADD
      || (operation == USERDEL && globalLdap->remove_home_directory))
    {
      /* homeDirectory is required by rfc 2307 for posixAccount */
      if (globalLdap->passent->pw_dir == NULL)
	{
	  char *hometemp = NULL;
	  size_t strsize = 0;
	  if (cfg_get_str ("LDAP", "HOME_DIRECTORY") == NULL)
	    {
	      fprintf (stderr, "ldap: populateGlobals: HOME_DIRECTORY not "
		       "found in config file or specified at command "
		       "line. Error.\n");
	      return -1;
	    }
	  else
	    {
	      strsize = strlen (cfg_get_str ("LDAP", "HOME_DIRECTORY")) +
		strlen (globalLdap->passent->pw_name) + 2;
	      hometemp = (char *) malloc (sizeof (char) * strsize);
	      if (hometemp == NULL)
		return -1;
	      bzero (hometemp, strsize);
	      snprintf (hometemp, strsize, "%s/%s",
			cfg_get_str ("LDAP", "HOME_DIRECTORY"),
			globalLdap->passent->pw_name);
	      globalLdap->passent->pw_dir = hometemp;
	    }
	}
    }

  if ( operation == USERDEL ) {
      if ( globalLdap->exec == NULL)
	  globalLdap->exec = cfg_get_str ("LDAP", "DEL_SCRIPT");
  }
  /* start populating required fields depending on requested operation */
  if (operation == USERADD)
    {
      /* gecos field is not required. rfc 2307 says MAY for posixAccount */
      if (globalLdap->passent->pw_gecos == NULL)
	globalLdap->passent->pw_gecos = cfg_get_str ("LDAP", "GECOS");

      if (globalLdap->exec == NULL)
	  globalLdap->exec = cfg_get_str ("LDAP", "ADD_SCRIPT");

      /* loginShell is not required by rfc 2307 */
      if (globalLdap->passent->pw_shell == NULL)
	globalLdap->passent->pw_shell = cfg_get_str ("LDAP", "DEFAULT_SHELL");

      if (globalLdap->make_home_directory
	  && globalLdap->skel_directory == NULL)
	globalLdap->skel_directory = cfg_get_str ("LDAP", "SKEL_DIR");

      object_vals = cfg_get_str ("LDAP", "USER_OBJECT_CLASS");
      if (object_vals == NULL)
	{
	  fprintf (stderr, "USER_OBJECT_CLASS was not found in the "
		   "configuration file and is required. Error.\n");
	  return -1;
	}
      while (object_vals != NULL && *object_vals)
	{
	  globalLdap->user_object_class =
	    (char **) realloc (globalLdap->user_object_class,
			       sizeof (char *) * ((num_tokens + 1) * 4));
	  globalLdap->user_object_class[num_tokens] =
	    getToken (&object_vals, ",");
	  num_tokens++;
	}
      globalLdap->user_object_class[num_tokens] = NULL;
    }
  if (operation == USERADD || operation == USERMOD)
    {
      if (operation == USERMOD)
	{
	  if (globalLdap->lock == true && globalLdap->unlock == true)
	    {
	      fprintf (stderr,
		       "An invalid request was made to lock and unlock the "
		       "users account.\n");
	      globalLdap->lock = false;
	      globalLdap->unlock = false;
	    }
	}
      if (globalLdap->passent->pw_passwd != NULL)
	{
	  if (globalLdap->hash == NULL)
	    globalLdap->hash = cfg_get_str ("LDAP", "HASH");
	  if (globalLdap->hash == NULL)	/* I don't like forcing this */
	    globalLdap->hash = "crypt";	/* crypt is pretty well supported */
	  if (globalLdap->shadow_file != NULL)
	    globalLdap->hash = "clear";
	  if (getHashType (globalLdap->hash) == H_UNKNOWN)
	    globalLdap->passent->pw_passwd =
	      getHash (H_CLEAR,
		       globalLdap->passent->pw_passwd,
		       globalLdap->hash, NULL);
	  else
	    globalLdap->passent->pw_passwd =
	      getHash (getHashType (globalLdap->hash),
		       globalLdap->passent->pw_passwd,
		       ldapGetHashPrefix (getHashType (globalLdap->hash)),
		       NULL);
	}
      else
	{
	  if (operation == USERADD)	/* must have a password */
	    {
	      globalLdap->passent->pw_passwd = getHash (getHashType ("clear"),
							"*", "{crypt}", NULL);
	    }
	}
    }
      if (globalLdap->user_base == NULL)
        {
          if ((globalLdap->user_base = cfg_get_str ("LDAP", "USER_BASE"))
              == NULL)
            {         
              fprintf (stderr, "USER_BASE not found in configuration file or "
                       "specified at command line but is required for "
                       "operation. Error.\n");
              return -1;
            }           
        }
  if ( cfg_get_str ("LDAP", "CN_STRING") ) {
    fprintf(stderr,
    	"Your configuration contains the CN_STRING option which is no longer\n"
	"supported. Please use USER_CN_STRING and GROUP_CN_STRING\n");

  }
  if (operation < 3 || operation == CAT)
    {
      globalLdap->cn = cfg_get_str ("LDAP", "USER_CN_STRING");
    }
  else if (operation > 2 && operation != CAT)
    {
      object_vals = cfg_get_str ("LDAP", "GROUP_OBJECT_CLASS");
      if (object_vals == NULL)
	{
	  fprintf (stderr, "GROUP_OBJECT_CLASS was not found in the "
		   "configuration file and is required. Error.\n");
	  return -1;
	}
      while (object_vals != NULL && *object_vals)
	{
	  globalLdap->group_object_class =
	    (char **) realloc (globalLdap->group_object_class,
			       sizeof (char *) * ((num_tokens + 1) * 4));
	  globalLdap->group_object_class[num_tokens] =
	    getToken (&object_vals, ",");
	  num_tokens++;
	}
      globalLdap->group_object_class[num_tokens] = NULL;
      globalLdap->cn = cfg_get_str ("LDAP", "GROUP_CN_STRING");
    }
  if (operation != CAT)
    globalLdap->dn = buildDn ((operation > 2) ? GROUPADD : USERADD,
			      globalLdap->passent->pw_name);

  if (globalLdap->add_file != NULL)
    {
      p = parseInit ();
      if (p == NULL)
	return -1;
      p = parseFile (p, globalLdap->add_file, ":", "#");
      globalLdap->parse = p;
    }
  return 0;
}

char *
buildDn (ldapop_t op, char *name)
{
  size_t slen = 0;
  char *temp;
  char *cn = NULL;

  /* FIXME: This is a bad way to determine the cn, since it is very
     conditional */
  if (operation == USERADD && op == GROUPADD)
    cn = cfg_get_str ("LDAP", "GROUP_CN_STRING");
  else
    cn = globalLdap->cn;
  if (cn == NULL)
    {
      if (op < 3)
	{
	  cn = cfg_get_str ("LDAP", "USER_CN_STRING");
	}
      else
	{
	  cn = cfg_get_str ("LDAP", "GROUP_CN_STRING");
	}
    }
  if (cn == NULL)
    cn = strdup ("cn");

  if (op < 3)
    {
      if (op != USERMOD)
	{
	  slen = strlen (name) +
	    strlen (globalLdap->user_base) + strlen (cn) + 3;
	}
      else
	{
	  slen = strlen (name) + strlen (cn) + 2;
	}
      temp = (char *) malloc (sizeof (char) * slen);
      if (temp == NULL)
	return NULL;
      bzero (temp, slen * sizeof (char));
      if (op != USERMOD)
	{
	  snprintf (temp, slen, "%s=%s,%s", cn, name, globalLdap->user_base);
	}
      else
	{
	  snprintf (temp, slen, "%s=%s", cn, name);
	}
    }
  else				/* group work */
    {
      if (op != GROUPMOD)
	{
	  slen = strlen (name) + strlen (cn) +
	    strlen (globalLdap->group_base) + 5;
	}
      else
	{
	  slen = strlen (name) + strlen (cn) + 2;
	}
      temp = (char *) malloc (sizeof (char) * slen);
      if (temp == NULL)
	return NULL;
      bzero (temp, slen * sizeof (char));
      if (op != GROUPMOD)
	{
	  snprintf (temp, slen, "%s=%s,%s", cn, name, globalLdap->group_base);
	}
      else
	{
	  snprintf (temp, slen, "%s=%s", cn, name);
	}
    }
  return temp;
}

void
printHelp (int op)
{
CTEST();
  fprintf (stderr,
	   "usage: cpu user{add,del,mod} [options] login\n"
	   "usage: cpu group{add,del,mod} [options] group\n"
	   "usage: cpu cat\n\n");
  switch (op)
    {
    case USERADD:
    case USERMOD:
    case USERDEL:
      printUserHelp (op);
      break;
    case GROUPADD:
    case GROUPMOD:
    case GROUPDEL:
      printGroupHelp (op);
      break;
    default:
      fprintf (stderr,
	       "LDAP Specific Options\n\n"
	       "\t-2, -2                           : If specified, use LDAPv2\n"
	       "\t-a addfile, --addfile=file       : File to use for additional attrs\n"
	       "\t-A cn, --cn=cn                   : Comman Name Prefix\n"
	       "\t-b base, --userbase=base         : Base DN for users\n"
	       "\t-B group_base, --groupbase=base  : Base DN for groups\n"
	       "\t-D bind_dn, --binddn=bind_dn     : Bind DN\n"
	       "\t-F[file], --passfile[=file]      : Password File\n"
	       "\t-H hash, --hash=hash             : Password Hash Type\n"
	       "\t-N hostname, --hostname=hostname : LDAP Server\n"
	       "\t-o, --nonposix                   : Allow names to violate POSIX naming\n"
	       "\t                                   conventions\n"
	       "\t-P port, --port=port             : Port\n"
	       "\t-R len, --random=len             : Length of a password to be\n"
	       "\t                                   randomly generated\n"
	       "\t-S[file], --shadfile[=file]      : Shadow Pass File\n"
	       "\t-t timeout, --timeout=timeout    : Timeout\n"
	       "\t-w[pass], --bindpass[=pass]      : Bind Password\n"
	       "\t-x, --tls                        : Use start tls\n"
	       "\t-Z uri, --uri=uri                : LDAP Uri\n\n"
	       "Attributes that can be added and modified\n\n"
	       "\t-f name, --firstname=name        : First Name\n"
	       "\t-e address, --email=address      : Email Address\n"
	       "\t-E name, --lastname=name         : Last Name\n\n"
	       "Options that impact all operations\n\n"
	       "\t-y, --yes                        : Yes to All\n"
	       "\t-h, --help                       : This help\n"
	       "\t-v, --verbose                    : Verbose\n"
	       "\t-V, --version                    : Version\n\n");
      break;
    }
  return;
}

void
printUserHelp (int op)
{
  switch (op)
    {
    case USERADD:
/* don't support -r -e -f -o */
      fprintf (stderr,
	       "usage: cpu useradd [options] login\n"
	       "\t-c comment --gecos=COMMENT       : Gecos Information\n"
	       "\t-d home_dir --directory=home_dir : Users home directory\n"
	       "\t-g initial_group --gid=initial   : The group id or name of the user's\n"
	       "\t                                   initial login group\n"
	       "\t-G group,[...] --sgroup=group,[] : A list of supplementary groups\n"
	       "\t-k[skeleton_dir] --skel[=dir]    : The skeleton directory\n"
	       "\t-m --makehome                    : The user's home directory will be\n"
	       "\t                                   created if it does not exist\n"
	       "\t-p[passwd] --password[=password] : The unencrypted password\n"
	       "\t-s shell --shell=shell           : The name of the user's login shell\n"
	       "\t-u uid --uid=uid                 : The numerical value of the user's\n"
	       "\t                                   ID.\n"
	       "\t-X script --exec=script          : Post add script\n\n");
      break;
    case USERMOD:
/* don't support -e -f -L -U -o */
      fprintf (stderr,
	       "usage: cpu usermod [options] login\n"
	       "\t-c comment --gecos=COMMENT       : Gecos Information\n"
	       "\t-d home_dir --directory=home_dir : Users home directory\n"
	       "\t-g initial_group --gid=integer   : The group id of the user's initial\n"
	       "\t                                   group\n"
	       "\t-G group,[...] --sgroup=group,[] : A list of supplementary groups\n"
	       "\t-l login_name, --newusername=NAME: New user name\n"
	       "\t-L --lock                        : Lock user account\n"
	       "\t-m --makehome                    : The user's old directory will be\n"
	       "\t                                   copied to the new location\n"
	       "\t-p passwd --password=password    : The unencrypted password\n"
	       "\t-s shell --shell=shell           : The name of the user's login shell\n"
	       "\t-u uid --uid=uid                 : The numerical value of the user's\n"
	       "\t                                   ID.\n"
	       "\t-U --unlock                      : Unlock user account\n\n");
      break;
    case USERDEL:
      fprintf (stderr,
	       "usage: cpu userdel [options] login\n"
	       "\t-r, --removehome                 : Remove user's home directory\n"
	       "\t-X script --exec=script          : Post remove script\n\n");
      break;
    default:
      break;
    }
  return;
}

void
printGroupHelp (int op)
{
  switch (op)
    {
    case GROUPADD:
/* don't support -o or -r */
      fprintf (stderr,
	       "usage: cpu groupadd [options] group\n"
	       "\t-g gid --gid=gid                 : The numeric value of the group id\n\n");
      break;
    case GROUPMOD:
/* don't support -o */
      fprintf (stderr,
	       "usage: cpu groupmod [options] group\n"
	       "\t-g gid --gid=gid                 : The numeric value of the group id\n"
	       "\t-n group_name --newgroupname=NAME: The name that group will change to\n\n");
      break;
    case GROUPDEL:
      fprintf (stderr, "usage: cpu groupdel group\n\n");
      break;
    default:
      break;
    }
  return;
}

void
printVersion (void)
{
  fprintf (stderr, "libcpu_ldap (CPU) %s\n"
	   "Written by Blake Matheny\n"
	   "Copyright 2001, 2002, 2003\n\n"
	   "This is free software; see the source for copying "
	   "conditions. There is NO\nwarranty; not even for "
	   "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n",
	   __VERSION);
  return;
}
