/*
     This file is part of CPU
     (C) 2003 Blake Matheny (and other contributing authors)

     CPU is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 2, or (at your
     option) any later version.

     CPU is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CPU; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * Print useful error messages for users
 * @author Blake Matheny
 * @file ldap_errors.c
 **/
#include <stdio.h>
#include <string.h>
#include "plugins/ldap/ldap.h"

#define SPC "     "

void
CPU_ldapPerror (LDAP * ld, CPU_ldap * opts, char *errmsg)
{
  int err = 0;
  char *ug = NULL;
  char *op = NULL;
  if (operation < 3)
    {
      ug = strdup ("user");
    }
  else
    {
      ug = strdup ("group");
    }
  switch (operation)
    {
    case USERADD:
    case GROUPADD:
      op = strdup ("add");
      break;
    case USERMOD:
    case GROUPMOD:
      op = strdup ("modify");
      break;
    case USERDEL:
    case GROUPDEL:
      op = strdup ("delete");
      break;
    }
  if (ldap_get_option (ld, LDAP_OPT_ERROR_NUMBER, &err) < 0)
    {
      fprintf (stderr, "Error in ldap_get_option\n");
      return;
    }
  fprintf (stderr, "CPU: %s: %s\n", errmsg, ldap_err2string (err));
  switch (err)
    {
    case LDAP_NO_SUCH_OBJECT:	/* err = 32 */
      fprintf (stderr,
	       "%sThe %s '%s' specified could not be found in the directory.\n",
	       SPC, ug, opts->passent->pw_name);
      fprintf (stderr,
	       "%sMake sure the %s is valid and the correct base for "
	       "the %s has been\n%sspecified.\n", SPC, ug, ug, SPC);
      break;
    case LDAP_INVALID_CREDENTIALS:	/* err = 49 */
      fprintf (stderr,
	       "%sThe credentials supplied ('%s','%s') were invalid.\n",
	       SPC, opts->bind_dn, "password");
      fprintf (stderr,
	       "%sIt is likely that the bind DN or password should be "
	       "changed.\n", SPC);
      break;
    case LDAP_ALREADY_EXISTS:	/* err = 68 */
      fprintf (stderr,
	       "%sThe %s you are trying to %s already exists in the "
	       "directory\n", SPC, ug, op);
      fprintf (stderr, "%sTry using a different %s name\n", SPC, ug);
      break;
    case LDAP_SERVER_DOWN:	/* err = 81 */
      fprintf (stderr,
	       "%sThe LDAP server specified at %s could not be contacted.\n",
	       SPC, (opts->uri == NULL) ? (opts->hostname) : (opts->uri));
      fprintf (stderr,
	       "%sYour LDAP server may be down or incorrectly specified.\n",
	       SPC);
      break;
    case LDAP_FILTER_ERROR:	/* err = 87 */
      fprintf (stderr,
	       "%sThe filter that was specified is invalid.\n", SPC);
      fprintf (stderr,
	       "%sIt is likely that either USER_FILTER or GROUP_FILTER "\
	       "is invalid.\n", SPC);
      break;
    default:
      fprintf (stderr,
	       "%sThe error number was %d, please add an appropriate entry to "
	       "%s.\n", SPC, err, __FILE__);
      fprintf (stderr,
	       "%sIf you are unable, please email %s with "
	       "the error number and what operation was being performed at "
	       "the time.\n", SPC, PACKAGE_BUGREPORT);
      break;
    }
  return;
}
