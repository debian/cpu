/*
     This file is part of CPU
     (C) 2003 Blake Matheny (and other contributing authors)

     CPU is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 2, or (at your
     option) any later version.

     CPU is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CPU; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * ldap plugin
 * @author Blake Matheny
 * @file ldap.c
 **/
#include <stdio.h>
#include "plugins/ldap/ldap.h"

int verbose;
int operation;

int
CPU_init (int argc, char *argv[])
{
  int ret;
  char * cmdstr = NULL;
  int cmdsize = 0;

  verbose = 0;
  operation = -1;

  if (initGlobals () < 0)
    return -1;

#ifdef DEBUG
  fprintf (stderr, "DEBUG: CPU_init: ldap: module loaded successfully\n");
#endif
  ret = parseCommand (argc, argv);
  if (ret < 0)
    {
#ifdef DEBUG
      fprintf (stderr,
	       "DEBUG: CPU_init: ldap: Error parsing command line.\n");
#endif
      return -1;
    }
  else if (ret == 1)
    {
      return 0;
    }

  if (populateGlobals () < 0)
    {
#ifdef DEBUG
      fprintf (stderr, "DEBUG: ldap: CPU_init: Error in populateGlobals.\n");
#endif
      return -1;
    }

  if (ldapOperation (operation) < 0)
    {
#ifdef DEBUG
      fprintf (stderr, "DEBUG: ldap: CPU_init: Error in ldapOperation.\n");
#endif
      return -1;
    }

  if (operation == USERDEL && globalLdap->remove_home_directory)
    {
      if (globalLdap->passent->pw_dir)
	{
	  remdir (globalLdap->passent->pw_dir);
	}
    }
  else if (operation == USERADD && globalLdap->make_home_directory)
    {
      if (globalLdap->passent->pw_dir && globalLdap->skel_directory)
	{
	  copy (globalLdap->skel_directory, globalLdap->passent->pw_dir,
		globalLdap->passent->pw_uid, globalLdap->passent->pw_gid);
	}
    }

  if ( (operation == USERADD || operation == USERDEL) &&
	globalLdap->exec != NULL )
  {
      cmdsize = sizeof(char) *
	  (strlen(globalLdap->exec) +
	   strlen(globalLdap->passent->pw_name) +
	   2);
      cmdstr = (char*)malloc(cmdsize);
      memset(cmdstr,0,cmdsize);
      snprintf(cmdstr, cmdsize, "%s %s", globalLdap->exec,
	      globalLdap->passent->pw_name);
      if ( system(cmdstr) == -1 ) {
	  fprintf(stderr,
		  "There was an error executing the command '%s'\n",
		  cmdstr);
	  return -1;
      }
  }
  return 0;
}
