/*
     This file is part of CPU
     (C) 2003 Blake Matheny (and other contributing authors)

     CPU is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 2, or (at your
     option) any later version.

     CPU is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with CPU; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * user{add,mod,del} functions and helpers
 * @author Blake Matheny
 * @file user.c
 **/
#include <stdio.h>
#include <string.h>
#include "plugins/ldap/ldap.h"

LDAPMod **userMod = NULL;

int
ldapUserAdd (LDAP * ld)
{
  if (ldapUserCheck (LDAP_MOD_ADD, ld) < 0)
    {
      fprintf (stderr, "ldap: ldapUserAdd: error in ldapUserCheck\n");
      return -1;
    }
  if (ldap_add_s (ld, globalLdap->dn, userMod) != LDAP_SUCCESS)
    {
      CPU_ldapPerror (ld, globalLdap, "ldapUserAdd: ldap_add_s");
      return -1;
    }
  fprintf (stdout, "User %s successfully added!\n",
	   globalLdap->passent->pw_name);
  return 0;
}

int
ldapUserMod (LDAP * ld)
{
  char *newdn = NULL;
  if (ldapUserCheck (LDAP_MOD_REPLACE, ld) < 0)
    {
      fprintf (stderr, "ldap: ldapUserMod: error in ldapUserCheck\n");
      return -1;
    }
  if (userMod == NULL && globalLdap->new_username == NULL)
    {
      fprintf (stderr, "ldap: ldapUserMod: No Modification requested\n");
      return 0;
    }

  if (globalLdap->new_username)
    {
      newdn = buildDn (USERMOD, globalLdap->new_username);
      if (newdn == NULL)
	return -1;
      if (ldap_modrdn2_s (ld, globalLdap->dn, newdn, 1) != LDAP_SUCCESS)
	{
	  CPU_ldapPerror (ld, globalLdap, "ldapUserMod: ldap_modrdn_s");
	  return -1;
	}
      free (newdn);
      if (globalLdap->make_home_directory && globalLdap->passent->pw_dir)
	{
	  fprintf (stderr, "Not yet implemented: stub\n");
	}
      globalLdap->passent->pw_name = globalLdap->new_username;
      newdn = buildDn (USERADD, globalLdap->new_username);
      globalLdap->dn = newdn;
    }

  if (userMod != NULL)
    {
      if (ldap_modify_s (ld, globalLdap->dn, userMod) != LDAP_SUCCESS)
	{
	  CPU_ldapPerror (ld, globalLdap, "ldapUserMod: ldap_modify_s");
	  return -1;
	}
    }

  fprintf (stdout, "User %s successfully modified!\n",
	   globalLdap->passent->pw_name);
  return 0;
}

char *
ldapGetPass (LDAP * ld)
{
  LDAPMessage *res[2];
  LDAPMessage *pos;
  BerElement *ber;
  char *filter;
  char *filter2;
  char *a;
  char **vals;
  size_t strsize;
  int i = 0;
  int ldapres = 0;
  char *attrs[2] = {
    "userPassword",
    NULL
  };

  /* get the home directory so it can be removed */
  filter = cfg_get_str ("LDAP", "USER_FILTER");
  if (filter == NULL)
    filter = strdup ("(objectClass=posixAccount)");
  strsize = strlen (filter) + strlen (globalLdap->passent->pw_name) + 11;
  filter2 = (char *) malloc (sizeof (char) * strsize);
  if (filter2 != NULL)
    {
      res[1] = NULL;
      bzero (filter2, strsize);
      snprintf (filter2, strsize, "(&%s (uid=%s))", filter,
		globalLdap->passent->pw_name);
      if (ldap_search_st (ld, globalLdap->user_base, LDAP_SCOPE_SUBTREE,
			  filter2, attrs, 0, &globalLdap->timeout, res)
	  != LDAP_SUCCESS)
	{
	  CPU_ldapPerror (ld, globalLdap, "ldapGetPass: ldap_search_st");
	}
      free (filter2);
      ldapres = ldap_count_entries (ld, res[0]);
      pos = ldap_first_entry (ld, res[0]);
      if (ldapres > 0)
	{
	  for (a = ldap_first_attribute (ld, pos, &ber); a != NULL;
	       a = ldap_next_attribute (ld, pos, ber))
	    {
	      if ((vals = ldap_get_values (ld, pos, a)) != NULL)
		{
		  for (i = 0; vals[i] != NULL; i++)
		    {
		      if (strncmp (a, "userPassword", 12) == 0)
			{
			  return strdup (vals[i]);
			}
		    }
		}
	    }
	}
    }
  return NULL;
}

int
ldapUserDel (LDAP * ld)
{
  LDAPMessage *res[2];
  LDAPMessage *pos;
  BerElement *ber;
  char *filter;
  char *filter2;
  char *a;
  char **vals;
  size_t strsize;
  int i = 0;
  int br = 0;
  int ldapres = 0;
  char *attrs[2] = {
    "homeDirectory",
    NULL
  };

  /* get the home directory so it can be removed */
  if (globalLdap->remove_home_directory)
    {
      filter = cfg_get_str ("LDAP", "USER_FILTER");
      if (filter == NULL)
	filter = strdup ("(objectClass=posixAccount)");
      strsize = strlen (filter) + strlen (globalLdap->passent->pw_name) + 11;
      filter2 = (char *) malloc (sizeof (char) * strsize);
      if (filter2 != NULL)
	{
	  res[1] = NULL;
	  bzero (filter2, strsize);
	  snprintf (filter2, strsize, "(&%s (uid=%s))", filter,
		    globalLdap->passent->pw_name);
	  if (ldap_search_st (ld, globalLdap->user_base, LDAP_SCOPE_SUBTREE,
			      filter2, attrs, 0, &globalLdap->timeout, res)
	      != LDAP_SUCCESS)
	    {
	      CPU_ldapPerror (ld, globalLdap, "ldapUserDel: ldap_search_st");
	    }
	  ldapres = ldap_count_entries (ld, res[0]);
	  pos = ldap_first_entry (ld, res[0]);
	  if (ldapres > 0)
	    {
	      for (a = ldap_first_attribute (ld, pos, &ber); a != NULL;
		   a = ldap_next_attribute (ld, pos, ber))
		{
		  if ((vals = ldap_get_values (ld, pos, a)) != NULL)
		    {
		      for (i = 0; vals[i] != NULL; i++)
			{
			  if (strncmp (a, "homeDirectory", 13) == 0)
			    {
			      globalLdap->passent->pw_dir = strdup (vals[i]);
			      br = 1;
			      break;
			    }
			}
		    }
		  if (br)
		    break;
		}
	    }
	}
    }
  if (ldap_delete_s (ld, globalLdap->dn) != LDAP_SUCCESS)
    {
      CPU_ldapPerror (ld, globalLdap, "ldapUserDel: ldap_delete_s");
      return -1;
    }
  fprintf (stdout, "User %s successfully deleted!\n",
	   globalLdap->passent->pw_name);
  return 0;
}

int
ldapUserCheck (int mod_op, LDAP * ld)
{
  int op = 0;
  char *tcn = NULL;

  if (mod_op == LDAP_MOD_ADD)
    op = LDAP_MOD_ADD;
  else if (mod_op == LDAP_MOD_REPLACE)
    op = LDAP_MOD_REPLACE;
  else
    return -1;

  /* the following is from rfc2307
     nisSchema.2.0 NAME 'posixAccount' SUP top AUXILIARY
     DESC 'Abstraction of an account with POSIX attributes'
     MUST ( cn $ uid $ uidNumber $ gidNumber $ homeDirectory )
     MAY ( userPassword $ loginShell $ gecos $ description )
   */
  if (op == LDAP_MOD_ADD)
    {
      userMod = ldapBuildListStr (LDAP_MOD_ADD, "cn", ldapGetCn (), userMod);
      userMod =
	ldapBuildList (op, "objectClass", globalLdap->user_object_class,
		       userMod);
    }

  userMod =
    ldapBuildListStr (op, "uid", globalLdap->passent->pw_name, userMod);

  /* do we allow duplicates ? */
  if ((int) globalLdap->passent->pw_uid > -1)
    userMod =
      ldapBuildListInt (op, "uidNumber", (int) globalLdap->passent->pw_uid,
			userMod);

  /* this doesn't work for gidNumber or memberUid
   * this needs its own function. Also, this shouldn't be done until after
   * ldap_add_s has completed successfully.
   * need to:
   *  search for gid in groups
   *   if it exists, add uid to group
   *   if it does not exist, continue?
   *  search for each memberUid
   *   if it exists, add uid to group
   *   if it does not exist, continue?
   */
  {
    LDAPMessage *res[2];
    LDAPMod *tmod[2];
    char *attrs[2] = {
      "gidNumber",
      NULL
    };
    char *filter2 = NULL;
    char *gf = NULL;
    char *mvals[2];
    int ldapres = 0;
    int lsize = 0;
    int strsize = 1;
    tmod[0] = (LDAPMod *) malloc (sizeof (LDAPMod));
    if (tmod[0] == NULL)
      return -1;
    bzero (tmod[0], sizeof (LDAPMod));

    mvals[0] = globalLdap->passent->pw_name;
    mvals[1] = NULL;

    tmod[0]->mod_op = LDAP_MOD_ADD;
    tmod[0]->mod_type = strdup ("memberUid");
    tmod[0]->mod_values = mvals;

    tmod[1] = NULL;
    res[1] = NULL;

    gf = cfg_get_str ("LDAP", "GROUP_FILTER");
    if (gf == NULL)
      gf = strdup ("(objectClass=posixGroup)");

    if ( (globalLdap->lock == true || globalLdap->unlock == true) &&
	 (op == LDAP_MOD_REPLACE) )
      {
	char *pass = ldapGetPass (ld);
	char *npass = NULL;
	char *tpass = NULL;
	int i, j;
	i = j = 0;
	if (pass != NULL)
	  {
	    npass = strdup (pass);
	    tpass = (char *) malloc (sizeof (char) * (strlen (npass) + 2));
	    bzero (tpass, sizeof (char) * (strlen (npass) + 2));
	  }
	else
	  {
	  }
	if (globalLdap->lock == true)
	  {
	    for (i = 0; i < (int) strlen (npass); i++)
	      {
		tpass[i] = npass[i];
		if (npass[i] == '}' && npass[i + 1] != '!')
		  {
		    tpass[i + 1] = '!';
		    for (j = i + 1; j < (int) strlen (npass); j++)
		      {
			tpass[j + 1] = npass[j];
		      }
		    globalLdap->passent->pw_passwd = tpass;
		    break;
		  }
	      }
	  }
	else if (globalLdap->unlock == true)
	  {
	    for (i = 0; i < (int) strlen (npass); i++)
	      {
		tpass[i] = npass[i];
		if (npass[i] == '}' && npass[i + 1] == '!')
		  {
		    for (j = i + 2; j < (int) strlen (npass); j++)
		      {
			tpass[j - 1] = npass[j];
		      }
		    globalLdap->passent->pw_passwd = tpass;
		    break;
		  }
	      }
	  }
      }
    else if ( globalLdap->lock == true || globalLdap->unlock == true )
	{
	    fprintf ( stderr,
		    "%slocking may only be used with usermod\n",
		    (globalLdap->lock==true)?"":"un" );
	    return -1;
	}
    if ((int) globalLdap->passent->pw_gid > -1)
      userMod = ldapBuildListInt (op, "gidNumber",
				  (int) globalLdap->passent->pw_gid, userMod);
    if (globalLdap->memberUid)
      {
	tcn = cfg_get_str ("LDAP", "GROUP_CN_STRING");
	if (tcn == NULL)
	  tcn = strdup ("cn");
	for (lsize = 0; globalLdap->memberUid[lsize] != NULL; lsize++)
	{
	    strsize = strlen (globalLdap->memberUid[lsize]) +
		strlen (gf) + strlen (tcn) + 8;
	    filter2 = (char *) malloc (sizeof (char) * strsize);
	    if (filter2 == NULL)
		return -1;
	    bzero (filter2, strsize);
	    snprintf (filter2, strsize, "(&%s (%s=%s))",
		    gf, tcn, globalLdap->memberUid[lsize]);
	    if (ldap_search_st
		    (ld, globalLdap->group_base, LDAP_SCOPE_SUBTREE, filter2,
		     attrs, 0, &globalLdap->timeout, res) != LDAP_SUCCESS)
	    {
		CPU_ldapPerror (ld, globalLdap,
			"ldapUserCheck: ldap_search_st");
		return -1;
	    }
	    ldapres = ldap_count_entries (ld, res[0]);
	    if (ldapres > 0)
	    {
		ldap_modify_s (ld, ldap_get_dn (ld, res[0]), tmod);
	    }
	}
      }
    if (globalLdap->gid)
    {
	tcn = cfg_get_str ("LDAP", "GROUP_CN_STRING");
	if (tcn == NULL)
	    tcn = strdup ("cn");
	strsize = strlen (globalLdap->gid) +
	    strlen (gf) + strlen (tcn) + 8;
	filter2 = (char *) malloc (sizeof (char) * strsize);
	if (filter2 == NULL)
	    return -1;
	bzero (filter2, strsize);
	snprintf (filter2, strsize, "(&%s (%s=%s))",
		gf, tcn, globalLdap->gid);
	if (ldap_search_st
		(ld, globalLdap->group_base, LDAP_SCOPE_SUBTREE, filter2,
		 attrs, 0, &globalLdap->timeout, res) != LDAP_SUCCESS)
	{
	    CPU_ldapPerror (ld, globalLdap,
		    "ldapUserCheck: ldap_search_st");
	    return -1;
	}
	ldapres = ldap_count_entries (ld, res[0]);
	if (ldapres > 0)
	{
	    ldap_modify_s (ld, ldap_get_dn (ld, res[0]), tmod);
	}
    }
    free (gf);
  }
  if (globalLdap->first_name)
    userMod =
      ldapBuildListStr (op, "givenName", globalLdap->first_name, userMod);

  if (globalLdap->last_name)
    userMod = ldapBuildListStr (op, "sn", globalLdap->last_name, userMod);

  if (globalLdap->new_username)
    userMod = ldapBuildListStr (op, "uid", globalLdap->new_username, userMod);

  if (globalLdap->email_address)
    userMod =
      ldapBuildListStr (op, "mail", globalLdap->email_address, userMod);

  if (globalLdap->passent->pw_passwd)
    userMod =
      ldapBuildListStr (op, "userPassword", globalLdap->passent->pw_passwd,
			userMod);

  if (globalLdap->passent->pw_gecos)
    userMod = ldapBuildListStr (op, "gecos", globalLdap->passent->pw_gecos,
				userMod);

  if (globalLdap->passent->pw_dir)
    userMod =
      ldapBuildListStr (op, "homeDirectory", globalLdap->passent->pw_dir,
			userMod);

  if (globalLdap->passent->pw_shell)
    userMod =
      ldapBuildListStr (op, "loginShell", globalLdap->passent->pw_shell,
			userMod);
  if ((int) globalLdap->passent->sp_lstchg != -10)
    userMod = ldapBuildListInt (op, "shadowLastChange",
				(int) globalLdap->passent->sp_lstchg,
				userMod);

  if ((int) globalLdap->passent->sp_min != -10)
    userMod =
      ldapBuildListInt (op, "shadowMin", (int) globalLdap->passent->sp_min,
			userMod);

  if ((int) globalLdap->passent->sp_max != -10)
    userMod =
      ldapBuildListInt (op, "shadowMax", (int) globalLdap->passent->sp_max,
			userMod);

  if ((int) globalLdap->passent->sp_warn != -10)
    userMod = ldapBuildListInt (op, "shadowWarning",
				(int) globalLdap->passent->sp_warn, userMod);

  if ((int) globalLdap->passent->sp_inact != -10)
    userMod = ldapBuildListInt (op, "shadowInactive",
				(int) globalLdap->passent->sp_inact, userMod);

  if ((int) globalLdap->passent->sp_expire != -10)
    userMod = ldapBuildListInt (op, "shadowExpire",
				(int) globalLdap->passent->sp_expire,
				userMod);

  if ((int) globalLdap->passent->sp_flag != -10)
    userMod = ldapBuildListInt (op, "shadowFlag",
				(int) globalLdap->passent->sp_flag, userMod);

  if (globalLdap->parse != NULL)
    {
      Parser *pos;
      pos = globalLdap->parse;
      while (pos != NULL)
	{
	  userMod = ldapBuildListStr (op, pos->attr, pos->attrval, userMod);
	  pos = pos->next;
	}
    }
  return 0;
}
